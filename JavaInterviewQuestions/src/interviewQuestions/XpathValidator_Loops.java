package interviewQuestions;

/*
 * Suppose you are building a utility in an automation framework to verify whether a given XPath expression is syntactically correct or not.Write a program that should identify and reject invalid syntax such as: Missing or unbalanced brackets Missing or unbalanced parentheses () Example 1:
 Input://div[@id='main Output: true
 and class='content J
•Explanation: The XPath expression is syntactically correct with valid path, attributes, and logical operators.

 Example 2:
 Input: //a[ehref='/home Output: false
 Explanation: The XPath expression is missing a closing bracket J for the attribute
 
Example 3:
• Input: /html/body//div[( • Output: false
 Explanation: The XPath expression has unbalanced parentheses.
 */

public class XpathValidator_Loops {

    public static boolean isValidXPath(String xpath) {
        int bracketCount = 0;
        int parenthesisCount = 0;

        for (int i = 0; i < xpath.length(); i++) {
            char ch = xpath.charAt(i);

            if (ch == '[') {
                bracketCount++;
            } else if (ch == ']') {
                if (bracketCount == 0) 
                	return false; // Unbalanced closing bracket
                bracketCount--;
            } else if (ch == '(') {
                parenthesisCount++;
            } else if (ch == ')') {
                if (parenthesisCount == 0) 
                	return false; // Unbalanced closing parenthesis
                parenthesisCount--;
            }
        }

        // Both counts should be zero
        return bracketCount == 0 && parenthesisCount == 0;
    }

    public static void main(String[] args) {
        System.out.println(isValidXPath("//div[@id='main' and @class='content']")); // true
        System.out.println(isValidXPath("//a[@href='/home'")); // false
        System.out.println(isValidXPath("/html/body//div[(")); // false
    }
}
