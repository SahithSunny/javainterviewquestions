package interviewQuestions;
/*
 * Q1: Suppose you are validating a mini search engine. 
 * Write a program to validate a search term can be broken down into a space-separated sequence of one or more tokens that are present in the search engine dictionary. 
 * Example 1: Search Terms = codereviens 
 * Search Engine Dict = ("code" , "crucial", "process", "reviews are development* of- part J Output: true 
 * Explanation: Return true because "codereviews can be segmented as "code reviews". 
 * Example 2: Search Terms = applepenapple Search Engine Dict = ["apple • pen"] Output: true 
 * Explanation: Return true because "applepenapple" can be segmented as "apple pen apple". 
 * Example 3: Search Terms= "integrationandeployment Search Engine Dict= "continous" , "integration", "and", "deployment" ] Output: false
 */

import java.util.*;

public class SearchEngineValidation {

    public static boolean validateSearchTerm(String s, List<String> dict) {
        // Convert the list of words into a set for faster lookup
        Set<String> wordDict = new HashSet<>(dict);
        
        // dp[i] is true if s[0:i] can be segmented into words from wordDict
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true; // An empty string is a valid segmentation

        // Check for each substring of s
        for (int i = 1; i <= s.length(); i++) {
            for (int j = 0; j < i; j++) {
                // If s[j:i] is in the dictionary and dp[j] is true, then dp[i] is true
                if (dp[j] && wordDict.contains(s.substring(j, i))) {
                    dp[i] = true;
                    break; // No need to check further if we found a valid segmentation
                }
            }
        }

        return dp[s.length()];
    }

    public static void main(String[] args) {
        // Example 1
        String searchTerm1 = "codereviews";
        List<String> dict1 = Arrays.asList("code", "crucial", "process", "reviews", "are", "development");
        System.out.println(validateSearchTerm(searchTerm1, dict1)); // Output: true
        
        // Example 2
        String searchTerm2 = "applepenapple";
        List<String> dict2 = Arrays.asList("apple", "pen");
        System.out.println(validateSearchTerm(searchTerm2, dict2)); // Output: true
        
        // Example 3
        String searchTerm3 = "integrationandeployment";
        List<String> dict3 = Arrays.asList("continuous", "integration", "and", "deployment");
        System.out.println(validateSearchTerm(searchTerm3, dict3)); // Output: false
    }
}
