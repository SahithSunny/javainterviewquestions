package interviewQuestions;

import java.util.Arrays;

/*
 * Given an array of N integers where each value represents the number of chocolates in  a packet. 
 * Each packet can have a variable number of chocolates. 
 * There are m students  the task is to distribute chocolate packets such that:  
� Each student gets one packet.  � The difference between the number of chocolates in the packet with  
maximum chocolates and the packet with minimum chocolates given to the students is minimum.  

example 1: Input :arr1 = {7, 3, 2, 4, 9, 12, 56} ,m=3  Output: Minimum Difference =  2 
Explanation:  We have seven packets of chocolates and we need to pick three packets for 3 students  If we pick 2, 3 and 4, 
we get the minimum difference between maximum and minimum  packet sizes.  

Example 2: Input : arr2 = {3, 4, 1, 9, 56, 7, 9, 12},  m=5  Output: Minimum Difference is 6  
If we pick 3,4,7,9,9 we get the minimum difference between maximum and minimum  packet sizes.

Algorithm:
- Sort the array.
- Traverse the sorted array and get diff between the 'i'-th element & 'i + m - 1'-th element for all i such that i + m - 1 is within bounds.
- Return the minimum difference.
 */

public class StringSubArray_FindMinDiff {
	
	public static int findMinDifference(int[] arr, int m, int n) {
		
		//return 0 if either the students 'm' or no.of chocolates in packet 'n' is 0
		if(n==0 || m==0) {
			return 0;
		}
		
		//Sort the given array
		Arrays.sort(arr);
		
        // Number of students cannot be more than the number of packets
		if(n<m) {
			return -1;
		}
		
		//find the min difference
		int minDiff = Integer.MAX_VALUE;
		
		for(int i=0; i+m-1<n ; i++) {
			int dif = arr[i+m-1]-arr[i];
			minDiff = Math.min(minDiff, dif);
		}
		
		return minDiff;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr1 = {7,3,2,4,9,12,56};
		int m1 =3;
		int n1 = arr1.length;
		System.out.println("Minimum differences is " + findMinDifference(arr1,m1,n1));
		
		int[] arr2 = {3, 4, 1, 9, 56, 7, 9, 12};
        int m2 = 5;
        int n2 = arr2.length;
        System.out.println("Minimum difference is " + findMinDifference(arr2, m2, n2));

	}

	

}
