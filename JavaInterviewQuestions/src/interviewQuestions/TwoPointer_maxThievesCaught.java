package interviewQuestions;
/*
 * Question 2  Given an array of size n that has the following specifications:  
 * Each element in the array contains either a policeman or a thief.  Each policeman can catch only one thief.  
 * A policeman cannot catch a thief who is more than K units away from the policeman  
 * We need to find the maximum number of thieves that can be caught.
 * example1: Input:arr[]={P',T, 'T', 'P', 'T}, k=1,  Output :2. 
 * Here maximum 2 thieves can be caught, first  policeman catches first thief and second police-  man can catch either second or third thief.  
 * example 2: Input :arr[]={T, 'T', 'P', P', 'T, 'P},  k=2.  Output:3.  
 * 
 */
import java.util.*;

public class TwoPointer_maxThievesCaught {

	//brute force approach
    public static int maxThievesCaught2(char[] arr, int n, int k) {
    	int count = 0;
    	
    	for(int i=0; i<n; i++) {
    		if(arr[i]=='P') {
    			int j = Math.max(0, i-k);
    			boolean flag = true;
    			for(int run=j; run<i; run++) {
    				if(arr[run]== 'T') {
    					arr[run] = 'C';
    					count++;
    					flag = false;
    					break;
    				}
    			}
    			if(flag == true) {
    				j=Math.min(i+k, n);
    				for(int run= i+1; run<=j; run++) {
    					if(arr[run]=='T') {
    						arr[run]='C';
    						count++;
    						flag = false;
    						break;
    					}
    				}
    			}
    		}
    	}
    	
    	return count;
    }
	
	
	
	
     public static int maxThievesCaught(char[] arr, int n, int k) {
        List<Integer> police = new ArrayList<>();
        List<Integer> thief = new ArrayList<>();
        
        // Store positions of police and thieves
        for (int i = 0; i < n; i++) {
            if (arr[i] == 'P') {
                police.add(i);
            } else if (arr[i] == 'T') {
                thief.add(i);
            }
        }
        System.out.println("police index ArrayList : "+police.toString());
        System.out.println("thief index ArrayList : "+thief.toString());

        int i = 0, j = 0, caughtThieves = 0;
        
        // Try to match police and thieves
        while (i < police.size() && j < thief.size()) {
        	
        	/*Math.abs() function returns the absolute value of the difference. 
        	 * This means it converts any negative difference to a positive value. For example:
        	 * If police.get(i) is 3 and thief.get(j) is 1, the result will be Math.abs(3 - 1) = 2.
        	 * If police.get(i) is 1 and thief.get(j) is 3, the result will still be Math.abs(1 - 3) = 2.
        	 * This absolute value ensures that we are only concerned with the distance between the policeman and thief, 
        	 * regardless of whether the policeman is to the left or right of the thief in the array.
        	 */
        	
        	// If the policeman can catch the thief (distance within range)
            if (Math.abs(police.get(i) - thief.get(j)) <= k) {
                caughtThieves++;
                i++;
                j++;
            }
            // If the policeman is behind the thief, move to the next policeman
            else if (police.get(i) < thief.get(j)) {
                i++;
            }
            // If the thief is behind the policeman, move to the next thief
            else {
                j++;
            }
        }
        
        return caughtThieves;
    }

    public static void main(String[] args) {
        char[] arr1 = {'P', 'T', 'T', 'P', 'T'};
        int k1 = 1;
        System.out.println("Max thieves caught: " + maxThievesCaught2(arr1, arr1.length, k1)); // Output: 2

        char[] arr2 = {'T', 'T', 'P', 'P', 'T', 'P'};
        int k2 = 2;
        System.out.println("Max thieves caught: " + maxThievesCaught2(arr2, arr2.length, k2)); // Output: 3
    }
}

