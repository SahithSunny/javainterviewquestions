package interviewQuestions;

import java.util.Arrays;

/*
 * Q: Given a string with numeric in between,  write program to reverse string alphabets but not numerics in java.
	eg: input String = a1b2c3d4e5  ; O/P: e1d2c3b4a5
	
	Approach:
	- Identify and store the positions of the alphabetic characters in the string.
	- Use two pointers to reverse only the alphabetic characters.
	- Construct the final string with reversed alphabets and original numerics.
	TIPS:
	- use !Character.isAlphabetic(chars[index]) - condition to skip the char other than alphabets
	- to convert the reverse array chars to string back - letterRev = new String(chars) OR letterRev = String.valueOf(chars)

 */

public class StringReversal_notNumerics {

	public static void main(String[] args) {
		
		String str = "a1b2c3d4e5";
		String letterRev = "";
		
		char[] chars = str.toCharArray();
		
		//declare left & right pointers
		int left = 0;
		int right = chars.length-1;
		
		
		//Loop untill two pointers met
		while(left < right) {
			
			//skip to next char if it's other than letter from left
			if( !Character.isAlphabetic(chars[left])) {
				left++;
			}
			else if(!Character.isAlphabetic(chars[right])){
				right--;
			}
			else {
				//swap two positions - if we found both as alphabets
				char temp = chars[left];
				chars[left] = chars[right];
				chars[right] = temp;
				
				left++;
				right--;
			}
		}
		
		// we can use String constructor also for the below conversion - letterRev = new String(chars)
		//to covert char array back to String 
		letterRev = String.valueOf(chars);
		System.out.println(str);
		System.out.println(letterRev);
		

	}

}
