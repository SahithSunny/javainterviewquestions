package mapInterface;

import java.util.HashMap;
import java.util.Map;

public class Min_Max_MapValues {

	public static void main(String[] args) {
		
		// Create a map with some data
        Map<String, Integer> map = new HashMap<>();
        map.put("apple", 3);
        map.put("banana", 10);
        map.put("cherry", 7);
        map.put("date", 11);
        
        //get iterator to get the first entry assigned to default min & max values
        Map.Entry<String, Integer> entry = map.entrySet().iterator().next();
        
        //consider firstValue as default min & max
        int minValue = entry.getValue();
        int maxValue = entry.getValue();
        
        
        for(Map.Entry<String, Integer> ent : map.entrySet()) {
        	int value = ent.getValue();
        	
        	//min value is
        	if(value < minValue) {
        		minValue = value;
        	}
        	if(value > maxValue) {
        		maxValue = value;
        		
        	}
        }
        
        System.out.println("minimumValue : "+minValue);
        System.out.println("maximumValue : "+maxValue);


	}

}
