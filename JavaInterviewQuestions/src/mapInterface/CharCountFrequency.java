package mapInterface;
/*
 * Meaning of 'getOrDefault(c, 0)' method:
The method getOrDefault(c, 0) checks if the map contains the key c (the character) or not.
If the key c exists in the map, it returns its associated value (the count of occurrences of that character).
If the key c does not exist in the map, it returns the default value, which is 0 in this case. 
This is useful because if the character hasn�t been counted yet, we treat its count as 0.

example; 
hm.getOrDefault('h', 0):
Since 'h' is not in the map, it returns the default value 0.
We then update the map by adding 'h' with the value 0 + 1 = 1.

o/p: hm = {'h': 1}
 */

import java.util.HashMap;
import java.util.Map;

public class CharCountFrequency {

	public static void main(String[] args) {
		
		String str = "malayalam";	
        // Create a HashMap to store character frequencies
		HashMap<Character,Integer> hm = new HashMap<Character,Integer>();
		
        // Loop through each character in the string
		for(char ele : str.toCharArray()) {
			if(hm.containsKey(ele)) {
				int count = hm.get(ele);
				
                // Increment count if character is already in the map
				hm.put(ele, count+1);
			}else {
				
                // Add character with initial count of 1
				hm.put(ele, 1);
			}
		}
		
		//print the map
		for(Map.Entry<Character, Integer> entry : hm.entrySet()) {
			System.out.println(entry.getKey() +" : "+ entry.getValue());
		}
		
		
		System.out.println();
		System.out.println("-------approach 2 ----------");
// approach 2 - using 'getOrDefault()' method
		
		HashMap<Character,Integer> hm2 = new HashMap<Character,Integer>();
		
		// Loop through each character in the string
        for (char c : str.toCharArray()) {
        	
            // Get the current count (default is 0) and add 1
            hm2.put(c, hm2.getOrDefault(c, 0) + 1);
        }
        
      //print the map
      	for(Map.Entry<Character, Integer> entry : hm2.entrySet()) {
      		System.out.println(entry.getKey() +" : "+ entry.getValue());
      	}
        
		
	}

}
