package mapInterface;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MergeTwoMaps {
	
	public static void main(String[] args) {
		
		//create first Map
		 Map<String, Integer> map1 = new HashMap<>();
	     map1.put("apple", 1);
	     map1.put("banana", 2);
	     
        // Create second map
	     Map<String, Integer> map2 = new HashMap<>();
	     map2.put("banana", 3);
	     map2.put("cherry", 4);
	     
	     //Create a new Map to merge both map1 & map2
	     Map<String, Integer> mergeMap = new LinkedHashMap<String,Integer>(map1);
	    // mergeMap.putAll(map2); //- this is direct way to merge, but it won't add common values O/P: apple-1, banana-3. cherry-4
	     
	     
	     // Iterate through the second map and merge entries into the merged map
	     for(Map.Entry<String, Integer> entry : map2.entrySet()) {
	    	 String key = entry.getKey();
	    	 Integer value = entry.getValue();
	    	 
	      // If the key is already in the mergedMap, sum the values - O/P: apple-1, banana-5 cherry-4
	    	 if(mergeMap.containsKey(key)) {
	    		 mergeMap.put(key, map1.get(key)+value);
	    	 }else {
	    		 mergeMap.put(key, value);
	    	 }
	     }
	     
	
	     
	     //loop map
	     for(Map.Entry<String, Integer> map : mergeMap.entrySet()) {
	    	 System.out.println(map.getKey() + " : "+map.getValue());
	     }
	}

}
