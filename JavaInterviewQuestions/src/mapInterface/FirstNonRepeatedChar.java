package mapInterface;

import java.util.HashMap;

public class FirstNonRepeatedChar {
	
	public static char FirstNonRepeatedChar(String str) {
		
		str = str.toLowerCase();
		
		HashMap<Character,Integer> hm = new HashMap<>();
		
        // First pass: store frequencies of each character
		for(char ele : str.toCharArray()) {
			
			//increase count if the element already present in hm
			if(hm.containsKey(ele)) {
				int count = hm.get(ele);
				hm.put(ele, count+1	);
			}else {
				
				//for the first time - add count as 1 by-default
				hm.put(ele, 1);
			}			
		}
		
		System.out.println(hm.keySet() + " : "+ hm.values());

		for(char c : str.toCharArray()) {
			if(hm.get(c) == 1) {
				return c;
			}
		}
		return 0;
		
		
	}


	public static void main(String[] args) {
		
		String str = "SwissW";
		char result = FirstNonRepeatedChar(str);
		
		System.out.println("first non repeated character in given string is : "+result);
		

	}

}
