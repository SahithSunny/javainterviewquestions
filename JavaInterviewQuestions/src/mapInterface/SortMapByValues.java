package mapInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/*
 * 1. create a map with some sample data
 * 2. convert the map to list of map entries
 * 3. sort the list using Map.Entry.ComparingByValues()
 * 4. Create a new LinkedHashMap & store values into it
 * 5. pint the sorted linkedHashMap
 */
public class SortMapByValues {

	public static void main(String[] args) {
		
        // Create a map with some data
		Map<String,Integer> mp = new HashMap<>();
		 mp.put("apple", 3);
	     mp.put("banana", 2);
	     mp.put("cherry", 5);
	     mp.put("date", 1);
	     
	    //convert map to ArrayList
	    List<Map.Entry<String,Integer>> li = new ArrayList<>(mp.entrySet());
	    
        // Sort the list using Map.Entry.comparingByValue() - for comparing values & compareByKet() - to compare keys
	    Collections.sort(li	, Map.Entry.comparingByValue());
	    
        // Create a new LinkedHashMap to store sorted entries
	    LinkedHashMap<String,Integer> lhm = new LinkedHashMap<>();
	    for(Map.Entry<String,Integer> entry : li) {
	    	lhm.put(entry.getKey()	, entry.getValue());
	    }
	    
	    //print the sorted linkedHashMap
	    for(Map.Entry<String, Integer> entry : lhm.entrySet()) {
	    	System.out.println(entry.getKey() + " : "+ entry.getValue());
	    }
	    
			

	}

}
