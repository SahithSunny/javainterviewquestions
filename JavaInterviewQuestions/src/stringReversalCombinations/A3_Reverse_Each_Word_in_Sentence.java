package stringReversalCombinations;

/*
 * Q: Reverse each word in a given sentence while keeping the word order the same.
Example: "Hello World" becomes "olleH dlroW"

approach:
- using split divide given string into the array of strings - loop the string array straight (left TO right) 
- inside loop use stringBuilder/buffer to reverse string  - concatenate reverse strings to final result with space at end & trim the result
 */

public class A3_Reverse_Each_Word_in_Sentence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// output = "olleH dlroW"
		
		String str = "Hello World";
		String result = "";
		StringBuffer sb;
		
		String[] word = str.split(" ");
		
		//loop reverse
		for(int i=0; i<=word.length-1; i++) {
			sb = new StringBuffer(word[i]);
			StringBuffer reverseWord = sb.reverse();
			
			result = result + reverseWord.toString() +" ";
		}
		
		System.out.println("given input string = "+str);
		System.out.println("reversal of whole word = "+result.trim());
				
	}

}
