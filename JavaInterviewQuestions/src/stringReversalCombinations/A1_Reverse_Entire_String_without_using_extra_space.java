package stringReversalCombinations;

/*
 * Q: Write a program to reverse an entire string without using additional space.
 * Eg: Original String: HelloWorld    ;   Reversed String: dlroWolleH
Approach:
To reverse an entire string without using additional space, we can convert the string to a character array 
and reverse it in place by swapping characters from both ends towards the center. 
This method is efficient because it doesn�t require any additional space other than the original character array.

TIPS:
- Use two pointer & use while till (left < right).
- to convert the reverse array chars to string back - letterRev = new String(chars) OR letterRev = String.valueOf(chars)
 */
public class A1_Reverse_Entire_String_without_using_extra_space {
	
	public static void main(String args[]) {
		
		String str = "HelloWorld";
		//Do not declare the reverse String, as we need to implement without using extra space
		
		char[] chars = str.toCharArray();
		
		int left = 0;
		int right = chars.length-1;
		
		while(left < right) {
			
			//swap two chars till both pointers meet
			char temp = chars[left];
			chars[left] = chars[right];
			chars[right] = temp;
			left++;
			right--;
		}
		
		System.out.println("Actual text = "+str);
		System.out.println("Reversal tect = "+ String.valueOf(chars));
	}
}
