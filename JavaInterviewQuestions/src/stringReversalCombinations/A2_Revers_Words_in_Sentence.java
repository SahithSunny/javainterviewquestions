package stringReversalCombinations;

/*
 * Q: Given a sentence, reverse the order of words while keeping each word's characters in order.
 Example: "Hello World" becomes "World Hello"
 
 Approach:
 Divide the words using split function & use loop in reverse order of the array count - 
 concatenate to reverse string with spaces after each join
 trim final result at end
 */

public class A2_Revers_Words_in_Sentence {

	public static void main(String[] args) {
		
		String str = "Hello World Achu!";
		String revStr = "";
		
		String[] words = str.split(" ");
		
		//loop from reverse length & concact to revString String variable
		for(int i=words.length-1; i>=0 ; i--) {
			
			revStr = revStr + words[i]+ " ";
			
		}
		
		System.out.println("Actual sring = "+str);
		System.out.println("Reverse string = "+revStr.trim());

	}

}
