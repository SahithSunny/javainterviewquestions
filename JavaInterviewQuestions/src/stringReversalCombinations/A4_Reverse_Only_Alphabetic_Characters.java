package stringReversalCombinations;

/*
 * Q: Reverse only the alphabetic characters in a string, keeping the positions of other characters (like numbers or symbols) fixed.
	Example: "a1b2c3d4" becomes "d1c2b3a4".
	
	Approach:
	- Identify and store the positions of the alphabetic characters in the string.
	- Use two pointers to reverse only the alphabetic characters.
	- Construct the final string with reversed alphabets and original numerics.
	TIPS:
	- use !Character.isAlphabetic(chars[index]) - condition to skip the char other than alphabets
	- to convert the reverse array chars to string back - letterRev = new String(chars) OR letterRev = String.valueOf(chars)

 */

public class A4_Reverse_Only_Alphabetic_Characters {

	public static void main(String[] args) {
		
		String str = "a1b2c3d4";
		char[] words = str.toCharArray();
		
		int left = 0;
		int right = words.length-1;
		
		//loop till both the pointers meet
		while(left < right) {
			
			//skip the char if it is not alphabet
			if(!Character.isAlphabetic(words[left])) {
				left++;
			}else if(!Character.isAlphabetic(words[right])) {
				right--;
			}else {
				// once we reach to position where both pointers are of alphabets then swap
				char temp = words[left];
				words[left] = words[right];
				words[right] = temp;
				left++;
				right--;
			}
			
			
		}
		
		System.out.println("Actual given string = "+str);
		System.out.println("Only alphabets reverse string = "+String.valueOf(words));
		

	}

}
