package listInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Q: Find the First Non-Repeating Element in a List
	  Given a List of integers, find the first element that does not repeat.
	  
Steps:
	1. Traverse the list once and use a HashMap to count the frequency of each element.
	2. Traverse the list again to find the first element with a frequency of 1.	  
 */
public class B5_FIrstNonRepeatingEle {
	public static int findFirstNonRepeating(List<Integer> list) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        
        // Count the frequency of each element
        for (int num : list) {
            int currentCount = 0; // Initialize currentCount
            
            // Check if the number is already in the map
            if (frequencyMap.containsKey(num)) {
                currentCount = frequencyMap.get(num); // Get the current count
                
                // Increment the count and put it back in the map
                frequencyMap.put(num, currentCount + 1);
            }else {
            	frequencyMap.put(num,  + 1);
            }
        }

        // Find the first element with frequency 1
        for (int num : list) {
            if (frequencyMap.get(num) == 1) {
                return num; // Return the first non-repeating element
            }
        }
        return -1; // Return -1 if no non-repeating element is found
    }
	
	

	public static void main(String[] args) {
		List<Integer> list = List.of(4, 5, 2, 4, 2, 5);
        System.out.println(findFirstNonRepeating(list)); // Output: 1

	}

}
