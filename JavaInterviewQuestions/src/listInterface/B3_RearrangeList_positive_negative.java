package listInterface;

import java.util.ArrayList;
import java.util.List;

/*
 * Q: Rearrange a List by Alternating Positive and Negative Numbers
	  Rearrange a List so that positive and negative numbers alternate, maintaining their relative order.

Steps:
	 1. Sort the list using a custom comparator that treats all negative numbers as smaller than positive numbers, 
	    but maintains relative order among positive and negative numbers.
     2. After sorting, interleave positive and negative numbers in the result.	  
	  
	  
 */
public class B3_RearrangeList_positive_negative {

	public static void rearrange(List<Integer> list) {
        // Separate positive and negative numbers
        List<Integer> positive = new ArrayList<>();
        List<Integer> negative = new ArrayList<>();

        for (int num : list) {
            if (num >= 0) {
                positive.add(num);
            } else {
                negative.add(num);
            }
        }

        // Merge the two lists while maintaining the order
        int i = 0, j = 0, k = 0;

        while (i < positive.size() && j < negative.size()) {
            if (k % 2 == 0) {
                list.set(k++, positive.get(i++));
            } else {
                list.set(k++, negative.get(j++));
            }
        }

        // Add remaining positive or negative elements
        while (i < positive.size()) {
            list.set(k++, positive.get(i++));
        }

        while (j < negative.size()) {
            list.set(k++, negative.get(j++));
        }
    }
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>(List.of(-1, 3, 5, 6, -7, 8, 2, -9));
        rearrange(list);
        System.out.println("Rearranged list: " + list);
        

	}

}
