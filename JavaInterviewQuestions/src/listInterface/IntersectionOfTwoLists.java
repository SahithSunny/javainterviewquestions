package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class IntersectionOfTwoLists {

	public static void main(String[] args) {
		
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> list2 = new ArrayList<> (Arrays.asList(3, 4, 5, 6, 7));
        System.out.println("Original list1 elements : "+list1.toString());
        System.out.println("Original list2 elements : "+list2.toString());

//approach 1 - using predefined method 'retainAll()'
// using this approach, it will modify the original list        
        
        list1.retainAll(list2);
        System.out.println("approach 1 - Intersection of both the lists is : "+list1);

// approach 2 - using HashSet class
// best approach for larger data - better performance as we are searching in set
        
        Set<Integer> hs = new HashSet<Integer>(list1);    // Convert list1 to a set for fast lookup
        List<Integer> resultList = new ArrayList<Integer>();
        
        for(int ele : list2) {
        	if(hs.contains(ele)) {
        		resultList.add(ele);    // Add common elements to intersection list
        	}
        }
        System.out.println("approach2 - intersection of two lists : "+resultList.toString());
        
// approach 3 - using java streams
        
        List<Integer> intersectionLis= list1.stream()
        									.filter(list2 :: contains)  // Keep elements present in both lists
        									.collect(Collectors.toList());
        
        System.out.println("approach3 - Intersection elements are : " + intersectionLis);

       					
	}

}
