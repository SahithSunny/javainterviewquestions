package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A1_AddElementsToArrayList {

	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		//Add elements to ArrayList
		list.add(20);
		list.add(22);
		list.add(24);
		list.add(26);
		list.add(28);
		list.add(30);
		
		//Printing ArrayList elements
		System.out.println(list.toString());
		
		System.out.println("------print using loop ---------");
		for(Object ele: list.toArray()) {
			System.out.println(ele);
		}
		
		

	}

}
