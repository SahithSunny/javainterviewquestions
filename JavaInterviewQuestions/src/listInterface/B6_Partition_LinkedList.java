package listInterface;

import java.util.ArrayList;
import java.util.List;



/*
 * Q: Partition a LinkedList Around a Value
	  Given a LinkedList and a value x, write a program to partition the list such that all nodes less than x 
	  come before nodes greater than or equal to x.
	  
Approach:
	 1. Traverse the linked list and store values in an array or list.
     2. Partition the values based on X
	 3. Rebuild the linked list from the partitioned values.
 */

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}

public class B6_Partition_LinkedList {
	
	public static ListNode partition(ListNode head, int x) {
        List<Integer> less = new ArrayList<>();
        List<Integer> greaterOrEqual = new ArrayList<>();

        // Traverse the original list
        while (head != null) {
            if (head.val < x) {
                less.add(head.val);
            } else {
                greaterOrEqual.add(head.val);
            }
            head = head.next; // Move to the next node
        }

        // Rebuild the linked list
        ListNode dummyHead = new ListNode(0);
        ListNode current = dummyHead;

        for (int val : less) {
            current.next = new ListNode(val);
            current = current.next;
        }
        for (int val : greaterOrEqual) {
            current.next = new ListNode(val);
            current = current.next;
        }

        return dummyHead.next; // Return the head of the partitioned list
    }

	public static void main(String[] args) {
		
		// Example: Create a linked list: 3 -> 5 -> 8 -> 5 -> 10 -> 2 -> 1
        ListNode head = new ListNode(3);
        head.next = new ListNode(5);
        head.next.next = new ListNode(8);
        head.next.next.next = new ListNode(5);
        head.next.next.next.next = new ListNode(10);
        head.next.next.next.next.next = new ListNode(2);
        head.next.next.next.next.next.next = new ListNode(1);

        // Partition around x = 5
        ListNode partitionedHead = partition(head, 5);

        // Print the partitioned list
        while (partitionedHead != null) {
            System.out.print(partitionedHead.val + " -> ");
            partitionedHead = partitionedHead.next;
        }
        System.out.println("null");
   
		
		

	}

}
