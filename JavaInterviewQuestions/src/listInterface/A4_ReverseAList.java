package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A4_ReverseAList {

	public static void main(String[] args) {

		List<String> list = Arrays.asList("cherry", "banana", "apple","jackFruit", "fig");
		System.out.println("original List: "+list);

		List<String> revlist = new ArrayList<String>();
		int n = list.size();
		
		for(int i=n-1; i>=0; i--) {
			revlist.add(list.get(i));
		}
		
		System.out.println("reverse List: "+revlist);
	}

}
