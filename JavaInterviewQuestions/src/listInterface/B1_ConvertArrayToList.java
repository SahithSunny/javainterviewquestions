package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class B1_ConvertArrayToList {

	public static void main(String[] args) {
		
		 Integer[] arr = {1, 2, 3, 4, 5, 6};
		 
//converting given array elements into list
		 List<Integer> li = new ArrayList<Integer>();
		 li = Arrays.asList(arr);
		 System.out.println("Array to list converted elements : "+li);
		 
		 // using java streams
		 int[] arr1 = {1, 2, 3, 4, 5, 6};
		 
		 List<Integer> li2 = (Arrays.stream(arr1)          // IntStream
                 .boxed()                                  // Stream<Integer>
                 .collect(Collectors.toList()));
		 
		 System.out.println("List using streams : " + li2);
		 

// converting List of elements into Array
		 	List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
	        Object[] array =  list.toArray();   //typeCasting into Integer wrapper class OR make array dataType as Object
	        System.out.println("Array: " + Arrays.toString(array));
		 
		 

	}

}
