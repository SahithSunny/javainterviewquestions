package listInterface;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * Step-by-Step Explanation
Creating the Person Class:

We define a class Person that has three fields: name, age, and salary.
A constructor is provided to initialize these fields.
We override the toString() method so that when we print a Person object, we get a readable string like: Alice - Age: 30, Salary: 50000.
Creating a List of Person Objects:

In the main() method, we create an ArrayList to store multiple Person objects.
We add some Person objects with different names, ages, and salaries to the list.
Sorting the List:

We use the sort() method of List to sort the list of Person objects.
Comparator.comparing(Person::getAge):
This tells Java to sort the list by the age field first.
.thenComparing(Person::getSalary):
If two people have the same age, this tells Java to sort by their salary.
The sorting is done automatically behind the scenes based on the criteria we defined.
Printing the Sorted List:

After sorting, we use forEach() to print each Person in the sorted list.
System.out::println prints each Person using the toString() method.

----------How the Sorting Works-------------
First, it compares the age of each Person. If one person is younger than another, they will appear first in the list.
If two people have the same age, then their salary is compared. The person with the lower salary will appear first.

 */

class Person {
    
	String name;
    int age;
    double salary;
    
    public Person(String name, int age, int salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
    
 // Getter methods for age and salary
    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }
    
    public String toString() {
        return name + " - Age: " + age + ", Salary: " + salary;
    }
    
    
    
}   



public class SortCustomObjects_multipleFIelds {

	public static void main(String[] args) {
//step - 1: Create a list of person objects
		List<Person> people = new ArrayList<>();
		
		//once the object is called - it will invoke the const & values been added to the class level variables
		people.add(new Person("Alice",29, 50000));
		people.add(new Person("Bob",19, 50000));
		people.add(new Person("Catrina",19, 40000));
		people.add(new Person("Dany",39, 50000));
		
		
//step - 2: Sort the list by age first, then by salary if ages are equal
		people.sort(Comparator.comparing(Person::getAge)    // Compare by age first
                .thenComparing(Person::getSalary));         // Compare by salary if ages are equal
		
		
// Step 3: Print the sorted list using a basic for loop
        for (Person person : people) {
            System.out.println(person);  // This will call the toString() method
        }


	}

}
