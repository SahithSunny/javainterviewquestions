package listInterface;

/*
 * approach - 1:
 * The simplest and most straightforward way is to use Java�s built-in retainAll() method, which retains only the elements that are common between two lists.
 * 
 * approach -2 
 * 1. Add all elements of list1 into a HashSet.
   2. Iterate through the other list and check if the element is present in the HashSet.
   3. If found, store the common elements into a separate 'ArrayList' & print arrayList
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class A8_CommonEleTwoArrayLists {

	public static void main(String[] args) {
	
		List<Integer> list1 = Arrays.asList(40,20,50,10,30);
		List<Integer> list2 = Arrays.asList(70,80,50,60,20);
		
		ArrayList<Integer> aList1 = new ArrayList<Integer>(list1);
		ArrayList<Integer> aList2 = new ArrayList<Integer>(list2);
		
		//first sort both the lists
		Collections.sort(aList1);
		Collections.sort(aList2);
		System.out.println("Original sorted List1 elements : "+aList1.toString());
		System.out.println("Original sorted List2 elements : "+aList2.toString());
		
//approach 1 - using retailAll() function
		aList1.retainAll(aList2); //eliminate all other elements which are not present in list2
		System.out.println("duplicate list elements : "+aList1);

		
		System.out.println("");
		System.out.println("---------approach 2 -----------");
//approach 2 - using the Set interface
		aList1 = new ArrayList<Integer>(list1);
		aList2 = new ArrayList<Integer>(list2);
		
		System.out.println("Original sorted List1 elements : "+aList1.toString());
		System.out.println("Original sorted List2 elements : "+aList2.toString());
		
		//FInd common elements using HashSet
		Set<Integer> hs = new HashSet<Integer>(aList1);
		
		List<Integer> dupElements = new ArrayList<Integer>();
		for(int ele : aList2) {
			if(hs.contains(ele)) {
				dupElements.add(ele);
			}
		}
		
		System.out.println("duplicate list elements : "+dupElements.toString());
		
		
	}

}
