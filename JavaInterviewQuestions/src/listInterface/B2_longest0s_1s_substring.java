package listInterface;

import java.util.HashMap;
import java.util.List;

/*
 * Q: Find the Longest Sublist with Equal 0s and 1s
	  Given a List of 0s and 1s, find the longest sublist with an equal number of 0s and 1s.
	  
Explanation:
  1. Convert all 0s to -1s in the list. Now, the problem is reduced to finding the longest sublist with a sum of 0.
  2. Maintain a cumulative sum while traversing the list.
  3. Use a HashMap to store the first occurrence of each cumulative sum. If the cumulative sum at two indices is the same, it means the elements between those two indices sum to 0.
 */

public class B2_longest0s_1s_substring {
	
	public static int findLongestSublist(List<Integer> list) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int maxLength = 0;
        int cumulativeSum = 0;

        // Initialize the HashMap with (0, -1) to handle sublist starting from index 0
        map.put(0, -1);

        for (int i = 0; i < list.size(); i++) {
            // Replace 0 with -1
            cumulativeSum += (list.get(i) == 0) ? -1 : 1;

            if (map.containsKey(cumulativeSum)) {
                // If the cumulative sum is already seen, calculate the sublist length
                int previousIndex = map.get(cumulativeSum);
                int sublistLength = i - previousIndex;
                maxLength = Math.max(maxLength, sublistLength);
            } else {
                // Store the first occurrence of the cumulative sum
                map.put(cumulativeSum, i);
            }
        }
        return maxLength;
    }

	public static void main(String[] args) {
		List<Integer> list = List.of(0, 0, 1, 0, 1, 1, 0);
		
        int result = findLongestSublist(list);
        System.out.println("The longest sublist with equal number of 0s and 1s has length: " + result);

	}

}
