package listInterface;

/*
 * Explanation:
Use two pointers, slow and fast.
Both pointers start at the head of the LinkedList.
The slow pointer moves one step at a time.
The fast pointer moves two steps at a time.
When the fast pointer reaches the end (or the last element in case of an odd-length list), the slow pointer will be at the middle element.
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class A5_FindMidEleOfLinkedList {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(20, 22, 24, 26, 28, 30, 12);
		LinkedList<Integer> ll = new LinkedList<Integer>(list);
		
		ListIterator<Integer> slow = ll.listIterator();
		ListIterator<Integer> fast = ll
				.listIterator();
		
		//move fast pointer to two steps and slow pointer to one step
		while(fast.hasNext()) {
			//move fast pointer as fast if possible
			fast.next();
			if(fast.hasNext()) {
				fast.next();
				slow.next(); //move slow pointer once - if fast pointer has possibility of second move
			}
		}
		
		//By the fast reaches to end, slow pointer will be in middle
		int mid = slow.next();
		System.out.println("Middle of ArrayList : "+mid);

	}

}
