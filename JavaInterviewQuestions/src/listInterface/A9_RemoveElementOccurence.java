package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A9_RemoveElementOccurence {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(50,20,50,10,30,50,90);
		List<Integer> aList = new ArrayList<Integer>(list);	
		System.out.println("Original ArrayList : "+aList.toString());
		
		List<Integer> ele = Arrays.asList(50);
		aList.removeAll(ele);
		System.out.println("Arraylist after removing element occurence : "+aList.toString());
		
		/*
		 * APPROACH - 2 - using Iterator
		//Remove all occurrences of 20 using Iterator
        Iterator<Integer> iterator = aList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == 50) {
                iterator.remove();
            }    
        System.out.println("Arraylist after removing element occurence : "+aList.toString());    
        }
		*/

	}

}
