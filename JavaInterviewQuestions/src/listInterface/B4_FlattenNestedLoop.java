package listInterface;

import java.util.ArrayList;
import java.util.List;

/*
 * Q: Flatten a Nested List
	  Write a program to flatten a List of Lists into a single List.
     
Steps:
    1. Create an empty result list.
    2. Traverse through the nested list, and for each sublist, iterate through its elements and add them to the result list.     
 */
public class B4_FlattenNestedLoop {
	
	public static List<Integer> flatten(List<List<Integer>> nestedList) {
        List<Integer> result = new ArrayList<>();

        // Traverse through each sublist
        for (List<Integer> sublist : nestedList) {
            // Add all elements of the sublist to the result list
            result.addAll(sublist);
        }

        return result;
    }

	public static void main(String[] args) {
		List<List<Integer>> nestedList = List.of(
	            List.of(1, 2, 3),
	            List.of(4, 5),
	            List.of(6, 7, 8, 9)
	        );

	    List<Integer> flattenedList = flatten(nestedList);
	    System.out.println(flattenedList); // Output: [1, 2, 3, 4, 5, 6, 7, 8, 9]
	}

}
