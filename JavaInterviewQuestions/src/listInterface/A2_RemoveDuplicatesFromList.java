package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class A2_RemoveDuplicatesFromList {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(20, 22, 24, 26, 28, 30, 26);
		
		Set<Integer> listAsSet = new HashSet<Integer>();
		for(int ele: list) {
			listAsSet.add(ele);
		}
		
		List<Integer> uniqueList = new ArrayList<Integer>(listAsSet);
		System.out.println("unique list :" +uniqueList.toString());
	}

}
