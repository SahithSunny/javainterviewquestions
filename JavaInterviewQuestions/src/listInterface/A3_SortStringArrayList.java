package listInterface;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class A3_SortStringArrayList {

	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("cherry", "banana", "apple","jackFruit", "fig");
		System.out.println("before sort : "+list.toString());

		//using sort(Comparator) function
		list.sort(Comparator.naturalOrder());
		System.out.println("Sorted list : "+list.toString());

		System.out.println("");
		list = Arrays.asList("cherry", "banana", "apple","jackFruit", "fig");
		System.out.println("before sort : "+list.toString());
		//using Collections.sort
		Collections.sort(list);
		System.out.println("Sorted list : "+list.toString());


	}

}
