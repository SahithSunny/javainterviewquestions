package listInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/*
 * approach 1: to loop & eliminate each matching value using index
 * 
 */

public class RemoveElement_list {

	public static void main(String[] args) {
		
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 3, 5, 3));
        int k = 3;
        System.out.println("original list is : "+list.toString());
        

// approach 1: to loop & eliminate each matching value using index
        for(int i=0; i<list.size(); i++ ) {
  
        	if(list.get(i) == k) {
        		list.remove(i);
        	}
        }
        
        System.out.println(" apprach 1- List after eliminating 'k' occurances is : "+list.toString());
	
// approach 2 - using removeAll() in a single go - recommanded appproach
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 3, 5, 3));

        List<Integer> removeEle = new ArrayList<Integer>();
        removeEle.add(k);
        list1.removeAll(removeEle);
        
        System.out.println(" apprach 2- List after eliminating 'k' occurances is : "+list1.toString());
        
 // approach 3 - using Iterator
        List<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 3, 5, 3));
        
        Iterator<Integer> itr = list2.iterator();
        while(itr.hasNext()) {
                	
        	if(itr.next() == k) {
        		itr.remove();
        	}
        }
        
        System.out.println(" apprach 3- List after eliminating 'k' occurances is : "+list2.toString());


	}

}
