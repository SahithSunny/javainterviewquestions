package listInterface;

/*
 * Approach:
1. Find the effective number of rotations: If the size of the list is n, rotating by k positions is the same as rotating by k % n 
(since rotating by n positions brings the list back to its original state).
2. Split the list into two parts:
   *The last k elements of the list.
   *The remaining first part of the list.
3. Rearrange the two parts: The last k elements come first, followed by the first part.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class A7_RotateByKelements {

	public static void main(String[] args) {
		
		List<Integer> list1 = Arrays.asList(40,20,50,10,90,60,50,30);
		List<Integer> list = new ArrayList<Integer>(list1);
		System.out.println("Original List without rotation : "+list.toString());

		
		int k= 3; //
		int n = list.size();
		
		//handling the case if 'k' is greater than the n
		if(k>n)
		k= k%n;
		
		//No rotation required - if k=0 OR n=1
		if(k==0 || n==1) {
			System.out.println("No rotation required ");
		}else {
			
			//split the list into two parts & first add 1-> "n-k to n" & next "0 to n-k"
			List<Integer> finalList = new ArrayList<Integer>();
			
			finalList.addAll(list.subList(n-k, n));
			finalList.addAll(list.subList(0, n-k));
			
			System.out.println("Final list after rotation of "+k+" elements : "+finalList.toString());
		}

	}

}
