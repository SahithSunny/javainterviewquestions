package listInterface;

/*
 * Two Pointers: We use two pointers, i and j, for list1 and list2 respectively. The pointers start at the beginning of each list.
Comparison: We compare the elements at the current positions of i and j. The smaller element is added to the mergedList, increment pointer.
Appending Remaining Elements: Once one of the lists is exhausted, the remaining elements from the other list are appended to the mergedList.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class A6_Merge2SortedLists {

	public static void main(String[] args) {
		
		List<Integer> list1 = Arrays.asList(40,20,50,10,30);
		List<Integer> list2 = Arrays.asList(70,80,50,60);
		
		List<Integer> mergeList = new ArrayList<Integer>();
		
		//First sort both the arrays
		Collections.sort(list1);
		Collections.sort(list2);
		
		System.out.println("sorted List1 : "+list1.toString());
		System.out.println("sorted List2 : "+list2.toString());

		
		
		//two pointers i and j
		int i=0, j=0;
		
		//traverse through both the lists & compare elements
		while(i<list1.size() && j<list2.size()) { //until the pointer reaches the size of both the lists
			
			if(list1.get(i) < list2.get(j)) {
				mergeList.add(list1.get(i));
				i++;
			}else {
				mergeList.add(list2.get(j));
				j++;
			}
			
			//add remaining elements of list1
			while(i<list1.size()) {
				mergeList.add(list1.get(i));
				i++;
			}
			
			
			//add remaining elements of list2
			while(j<list2.size()) {
				mergeList.add(list2.get(j));
				j++;
			}
			
			System.out.println("Merged List : "+mergeList.toString());
			
		}
		
		
		

	}

}
