package mostAskedCodingQuestions_1;

/*
 * Q: Given an expression, we need to calculate the reslut of given expression - considering the precedence priority
 * 
 * * Algorithm:
Here’s the algorithm broken down:
  1. Numbers are pushed onto the operand stack.
  2. Operators are pushed onto the operator stack. If an operator of lower precedence is encountered, evaluate the stack before pushing it.
  3. Left Parentheses ( are simply pushed onto the operator stack.
  4. Right Parentheses ) trigger the evaluation of everything inside the parentheses.
 */

import java.util.Stack; 
  
public class A1_Expression_Evaluator_RED 
{ 
    public static int evaluate(String expression) 
    { 
        char[] ch = expression.toCharArray(); 
  
         // Stack for numbers: 'values' 
        Stack<Integer> values = new Stack<Integer>(); 
  
        // Stack for Operators: 'ops' 
        Stack<Character> ops = new Stack<Character>(); 
  
        for (int i = 0; i < ch.length; i++) 
        { 
             // Current token is a whitespace, skip it 
            if (ch[i] == ' ') 
                continue; 
  
            // Current token is a number, push it to stack for numbers 
            if (ch[i] >= '0' && ch[i] <= '9') 
            { 
                StringBuffer sbuf = new StringBuffer(); 
                // There may be more than one digits in number 
                while (i < ch.length && ch[i] >= '0' && ch[i] <= '9') 
                    sbuf.append(ch[i++]); 
                values.push(Integer.parseInt(sbuf.toString())); 
            } 
  
            // Current token is an opening brace, push it to 'ops' 
            else if (ch[i] == '(') 
                ops.push(ch[i]); 
  
            // Closing brace encountered, solve entire brace 
            else if (ch[i] == ')') 
            { 
                while (ops.peek() != '(') 
                  values.push(applyOp(ops.pop(), values.pop(), values.pop())); 
                ops.pop(); 
            } 
  
            // Current token is an operator. 
            else if (ch[i] == '+' || ch[i] == '-' || 
                     ch[i] == '*' || ch[i] == '/') 
            { 
                // While top of 'ops' has same or greater precedence to current 
                // token, which is an operator. Apply operator on top of 'ops' 
                // to top two elements in values stack 
                while (!ops.empty() && hasPrecedence(ch[i], ops.peek())) 
                  values.push(applyOp(ops.pop(), values.pop(), values.pop())); 
  
                // Push current token to 'ops'. 
                ops.push(ch[i]); 
            } 
        } 
  
        // Entire expression has been parsed at this point, apply remaining 
        // ops to remaining values 
        while (!ops.empty()) 
            values.push(applyOp(ops.pop(), values.pop(), values.pop())); 
  
        // Top of 'values' contains result, return it 
        return values.pop(); 
    } 
  
    // Returns true if 'op2' has higher or same precedence as 'op1', 
    // otherwise returns false. 
    public static boolean hasPrecedence(char op1, char op2) 
    { 
        if (op2 == '(' || op2 == ')') 
            return false; 
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-')) 
            return false; 
        else
            return true; 
    } 
  
    // A utility method to apply an operator 'op' on operands 'a'  
    // and 'b'. Return the result. 
    public static int applyOp(char op, int b, int a) 
    { 
        switch (op) 
        { 
        case '+': 
            return a + b; 
        case '-': 
            return a - b; 
        case '*': 
            return a * b; 
        case '/': 
            if (b == 0) 
                throw new
                UnsupportedOperationException("Cannot divide by zero"); 
            return a / b; 
        } 
        return 0; 
    } 
  
    // Driver method to test above methods 
    public static void main(String[] args) 
    { 
       // System.out.println(A1_Expression_Evaluator_RED.evaluate("10 + 2 * 6")); 
     //   System.out.println(A1_Expression_Evaluator_RED.evaluate("100 * 2 + 12")); 
      //  System.out.println(A1_Expression_Evaluator_RED.evaluate("100 * ( 2 + 12 )")); 
        System.out.println(A1_Expression_Evaluator_RED.evaluate("100 * ( 2 + 12 ) / 14")); 
    } 
}