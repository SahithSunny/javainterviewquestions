package mostAskedCodingQuestions_1;
/*
 * Q: Given an array - FInd the maximum product pair in array whole array
 * 
 * Approach:
 * 
 */
public class A4_maxProductArrayOfposnegInteger {

	public static void main(String[] args) {
		int arr[] = {1, -10, -20, 5, 4};
        int n = arr.length;
        maxProduct(arr, n);
        System.out.println("-----------optimal solution -----------");
        optimalMaxProduct(arr, n);

	}
	
	
//brute-force approach - TC:O(n3)
	static void maxProduct(int[] arr, int n) {
		
		if(n<2) {
			return;
		}
		
		//initializing max product pair
		int a = arr[0]; int b = arr[1]; int c =arr[2];
		
		//traverse through every possible pair and keep track of max product as max
		for(int i=0; i<n; i++) {
			for(int j=i+1; j<n; j++) {
				for(int k=j+1; k<n; k++) {
					
					if((arr[i]* arr[j]* arr[k] ) > a*b*c ) {
						a=arr[i];
						b=arr[j];
						c=arr[k];
					}
				}
				
			}
		}
		System.out.println("max product values are : "+ a+","+b+","+c);
		System.out.println("max product is : "+ a*b*c);
		
	}
	
	
//optimal solution - TC:O(n)

    // Function to find maximum product of three numbers
    static void optimalMaxProduct(int arr[], int n) {
        if (n < 3) {
            System.out.println("No triplet exists");
            return;
        }

        // Initialize the three largest and two smallest values
        int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE;
        int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;

        // Traverse the array and track the required values
        for (int i = 0; i < n; i++) {
            // Update the largest values
            if (arr[i] > max1) {
                max3 = max2;
                max2 = max1;
                max1 = arr[i];
            } else if (arr[i] > max2) {
                max3 = max2;
                max2 = arr[i];
            } else if (arr[i] > max3) {
                max3 = arr[i];
            }

            // Update the smallest values
            if (arr[i] < min1) {
                min2 = min1;
                min1 = arr[i];
            } else if (arr[i] < min2) {
                min2 = arr[i];
            }
        }

        // Maximum product can be:
        // 1. Product of the three largest numbers
        // 2. Product of two smallest (most negative) numbers and the largest number
        int product1 = max1 * max2 * max3;
        int product2 = min1 * min2 * max1;

        // Find and print the larger of the two products
        if (product1 > product2) {
            System.out.println("Max product triplet is {" + max1 + ", " + max2 + ", " + max3 + "}");
        } else {
            System.out.println("Max product triplet is {" + min1 + ", " + min2 + ", " + max1 + "}");
        }

        System.out.println("Maximum product: " + Math.max(product1, product2));
    }

}
