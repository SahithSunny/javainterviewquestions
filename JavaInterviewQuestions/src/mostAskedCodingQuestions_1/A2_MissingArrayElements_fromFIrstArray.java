package mostAskedCodingQuestions_1;

import java.util.HashSet;

/*
 * Q: Given 2 Arrays , Array 'A' & Array 'B'- 
 * Find the Missing Elements from Array 'A' which are not present in second Array 'B'
 * 
 * Approach:
 * 1. We will create an empty HashSet & add all second array elements into 'HashSet'
 * 2. Then we compare the first array elements - if not present in HashSet (second array)- Then print those missing elements from array 'A' as output
 * 
 * Eg: int a[] = { 1, 2, 6, 3, 4, 5 };  
       int b[] = { 2, 4, 3, 1, 0 }; 
       O/P = 6 5
 */

public class A2_MissingArrayElements_fromFIrstArray {

	public static void main(String[] args) {
		int a[] = { 1, 2, 6, 3, 4, 5 };  
	    int b[] = { 2, 4, 3, 1, 0 }; 
	    int m = a.length;
	    int n = b.length;
	    
	    missingElements(a,b,m,n);
	
	}

	static void missingElements(int[] a, int[] b, int m, int n) {
		// create an HashSet & add second array elements into HashSet
		HashSet<Integer> hs = new HashSet<Integer>();
		
		for(int i=0; i<n; i++) {
			hs.add(b[i]);
		}
		
		
		//compare the each element from array A- if it's not present is hashset - then print those missing elements from Array 'A'
		for(int i=0; i<m; i++) {
			if(!hs.contains(a[i])) {
				System.out.print(a[i]+ " ");
			}
		}
		
	}
	
	
	
}
