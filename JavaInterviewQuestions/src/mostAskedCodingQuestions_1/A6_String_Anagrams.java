package mostAskedCodingQuestions_1;

import java.util.Arrays;

/*
 * Q:  function to check whether two strings are anagram of each other 
 * str1 = "listen";
 * str2 = "silent";
 * Approach:
 * 1. First compare length - if length not matches no anagrams
 * 2. sort both the strings and compare each char from both the strings
 */

public class A6_String_Anagrams {

	public static void main(String[] args) {
		
		String str1 = "listen";
		String str2 = "silent";
		
		System.out.println("Strings anagram : "+checkAnagram(str1, str2));

	}

	static boolean checkAnagram(String str1, String str2) {
		
		boolean flag = true;
		
		//check if both the string lengths are equal or not
		if(str1.length() != str2.length()) {
			flag = false;
		}else{
			
			char[] chStr1 = str1.toCharArray();
			char[] chStr2 = str2.toCharArray();
			
			Arrays.sort(chStr1);
			Arrays.sort(chStr2);
			
			for(int i=0; i<str1.length(); i++) {
				
				if(chStr1[i] != chStr2[i]) {
					flag = false;
					break;
				}
			}
		}
		return flag;	
		}
		
	}


