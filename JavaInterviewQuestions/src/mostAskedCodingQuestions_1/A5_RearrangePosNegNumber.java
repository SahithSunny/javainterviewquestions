package mostAskedCodingQuestions_1;

/*
 * Q: Given an array - rearrange the elements of a given array so that positive elements are placed at even indexes (0, 2, …) and negative elements at odd indexes (1, 3, …)
 * 
 * Approach: two-pointer approach:
 * 1. Use two pointers, posIndex starting at 0 and negIndex starting at 1.
   2. Traverse the array:
		If the element at posIndex is positive, move posIndex to the next even index.
		If the element at negIndex is negative, move negIndex to the next odd index.
		If they are in the wrong places, swap the elements at posIndex and negIndex.
 */

public class A5_RearrangePosNegNumber {

	public static void main(String[] args) {
		int arr[] = {1, -3, 5, 6, -2, -8, -9, -4};
        int n = arr.length;
        rearrange(arr, n);

        // Print the rearranged array
        for (int i : arr)
            System.out.print(i + " ");

	}

	static void rearrange(int[] arr, int n) {
		
		int posIndex = 0;
		int negIndex = 1;
		
		// Function to rearrange array such that positive
	    // elements are placed at even indices and negative
	    // elements at odd indices.
		
		while(posIndex < n && negIndex < n) {
			
            // If positive is in the correct place
			if(arr[posIndex] >= 0) {
				posIndex += 2;
				
	        // If positive is in the correct place	
			}else if(arr[negIndex] < 0) {
				negIndex += 2;
			}else {
				
	        // If they are in the wrong places, swap
				int temp = arr[posIndex];
				arr[posIndex] = arr[negIndex];
				arr[negIndex] = temp;
			}	
		}
	}

}
