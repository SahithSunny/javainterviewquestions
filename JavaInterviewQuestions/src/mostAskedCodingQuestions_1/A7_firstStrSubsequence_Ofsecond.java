package mostAskedCodingQuestions_1;

/*
 * Q: solve the problem of determining if str1[] is a subsequence of str2[]. 
 * A subsequence means that all the characters of str1[] appear in str2[] in the same order, but not necessarily consecutively
 * eg: str1 = "abc";
       str2 = "ahbgdc";
       
 approach:
1. Initialize two pointers: one for str1[] (i = 0) and one for str2[] (j = 0).
2. Traverse str2[] using j. Whenever characters from str1[] match the current character in str2[], move i forward.
3. If the entire str1[] is traversed (i == m), return true. If str2[] is exhausted and i is not equal to m, return false.      
 */

public class A7_firstStrSubsequence_Ofsecond {

	public static void main(String[] args) {
		
		String str1 = "gksr%ek@@@"; 
        String str2 = "geeksforge%eks@@@"; 
        int m = str1.length(); 
        int n = str2.length(); 
        boolean result = isSubSequence(str1, str2, m, n); 
        
        if(result) {
        	System.out.println("str1 is subsequence of str2");
        }else {
        	System.out.println("str1 is NOT subSequence of str2");
        }

	}

	
	static boolean isSubSequence(String str1, String str2, int m, int n) {
		boolean res = true;
		
		if(m>n) {
			res = false;
		}else {
			
			int i=0;
			int j=0;
			
			// run the loop till all the characters in either str1 / either str2 are checked
			while(i<m && j<n) {
				
				if(str1.charAt(i) == str2.charAt(j)) {
					i++;
				}
				//always increment the next element in str2 chars
				j++;
			}
			
			// If all characters of str1 were found in str2 
	        res =  (i == m);  
		}
		return res;
	}

}
