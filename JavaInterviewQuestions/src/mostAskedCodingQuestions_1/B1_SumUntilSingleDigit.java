package mostAskedCodingQuestions_1;

/*
 * Q: Given an integer number: find the sum of the number untill the value becomes single digit
 * Also called as digital root of the given number
 * EG: For 2298, you first calculate the sum of its digits: 2+2+9+8=21
	   Then, you sum the digits of 21: 2+1=3.
	   Since 3 is a single digit, the function returns 3.
	   
Approach:
1. Add the last digit of n to sum (by calculating n % 10).
2. Divide n by 10 to remove the last digit.
3. If n becomes zero, set n equal to sum, reset sum to 0, and continue.	   

 */

public class B1_SumUntilSingleDigit {

	public static void main(String[] args) {
		
		int numb = 2298;
		int sum=0;
		
		//Loop until we reduce the number to a single digit
		while(numb>0 || sum>9) {
			
			//once all digits are summed, means n=0  --> then set n=sum & rest sum =0
			if(numb==0) {
				numb = sum;
				sum = 0;
			}
			
			sum = sum+ numb%10;
			numb = numb/10;
		}
		
		System.out.println(sum);
		
	}

}
