package mostAskedCodingQuestions_1;

import java.util.HashSet;

/*
 * Q: Given an array Elements and a sum value- find the maximum possible pairs count & pair-values in Array
 * Which equals to the sum value
 * 
 * Approach:
 * 1. loop through the first array element - comparing with all other elements of i+1
 * 2. if both values == sum - then increment the counter & print the pair values
 */

public class A3_PairElements_EqualsToSum {

	public static void main(String[] args) {
		
		int arr[] = { 1, 5, 7, -1, 5 }; 
		int n = arr.length; 
		int sum = 6; 
		System.out.println("no.of pairs are : "+printPairs(arr, n, sum)); 
		
		System.out.println("Approach 2 - using hashset");
		System.out.println("no.of pairs are : "+optimisedprintPairs(arr, n, sum)); 

	}

//brute-force approach - TC: O(n2)
	static int printPairs(int[] arr, int n, int sum) {
		
		int counter = 0;
		
		//outer-loop for first element
		for(int i=0; i<=n; i++) {
			//inner loop for next all elements
			for(int j=i+1; j<n; j++) {
				//if both pair values equal to sum - then print the pairs
				if(arr[i]+ arr[j] == sum) {
					counter++;
					System.out.println("("+arr[i]+","+arr[j]+")"+" ");
				}
			}
		}
		return counter;
	}
	
	

	
//optimised approach - using HashSet - TC: O(n)
	
    static int optimisedprintPairs(int[] arr, int n, int sum) {
        // HashSet to store the elements of the array
        HashSet<Integer> hs = new HashSet<>();
        
        int count = 0;
        
        //traverse through array
        for(int i=0; i<n; i++) {
        	// Find the complement of arr[i] (i.e., sum - arr[i])
        	int complement = sum-arr[i];
        	
        	// If complement exists in the set, we've found a pair
            if (hs.contains(complement)) {
                count++;
                System.out.println("(" + complement + ", " + arr[i] + ")");
            }
            
            // Add the current element to the set
            hs.add(arr[i]);
        }
		return count;
        
    }     

}
