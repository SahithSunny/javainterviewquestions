package mostAskedCodingQuestions_1;

import java.util.Arrays;

/*
 * Q: Given an shuffled array with 0's and 1's. re-arrange the elements such that All 0's towards left & all 1's towards right
 * EG: given arr: {0, 1, 0, 1, 0, 0, 1, 1, 1, 0} ; result arr: {0,0,0,0,0,1,1,1,1,1};
 * 
 * Approach: using two-pointer
 *  1. Use two pointers, one starting from the left and the other from the right.
	2. If the left pointer finds a 1 and the right pointer finds a 0, swap them.
	3. Continue this until the two pointers meet.
	
Approach 2:
	1. Initialize a count variable to store the number of 0s.
	2. Traverse the array and increment the count for every 0 found.
	3. Traverse the array again and fill the first count positions with 0s.
	4. Fill the remaining positions with 1s.	
 * 
 */

public class B2_SegreggateArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int arr[] = {0, 1, 0, 1, 0, 0, 1, 1, 1, 0};
        segregate(arr);
        System.out.println(Arrays.toString(arr));

	}
	
	
	public static void segregate(int arr[]) {
        int left = 0, right = arr.length - 1;
        
        while (left < right) {
        	
        	// for below while conditions - If condition will not give the desired output
            // Move the left pointer to the right until we find a 1
            while (arr[left] == 0 && left < right) {
                left++;
            }
            // Move the right pointer to the left until we find a 0
            while (arr[right] == 1 && left < right) {
                right--;
            }
            // If left < right, swap arr[left] and arr[right]
            if (left < right) {
                arr[left] = 0;
                arr[right] = 1;
                left++;
                right--;
            }
        }
    }

}
