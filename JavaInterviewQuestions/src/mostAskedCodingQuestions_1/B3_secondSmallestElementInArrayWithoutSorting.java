package mostAskedCodingQuestions_1;

import javax.swing.text.StyleContext.SmallAttributeSet;

/*
 * Q: Finding the second largest element in an array
 * eg: arr = {12, 35, 1, 10, 34, 1} - o/p: 10
 * 
 * Approach: - optimal approach using single loop
 *	1. Traverse the array while keeping track of the smallest and second-smallest elements.
	2. For each element, update the smallest or second-smallest accordingly.
	
	3. Edge cases  - if array size is <2 & if array is identical array
 * 
 */

public class B3_secondSmallestElementInArrayWithoutSorting {

	public static void main(String[] args) {
		int[] arr = {-9, 35, 1, 10, 34, 1};
		
		System.out.println("Second small array element is : "+ CalcSecondSmallestArrayElem(arr));
	}
	
	
	//function to calculate the second smallest element in array
	static int CalcSecondSmallestArrayElem(int[] arr) {
		int firstSmall = Integer.MAX_VALUE;
		int secondSmall = Integer.MAX_VALUE;
		
		//edge case if the array elements is only 1 or empty array
		if(arr.length < 2) {
			System.out.println("Array elements are 1 or 0");
			return -1;
		}
		
		
		//traverse through the array elements
		for(int ele: arr) {
			
			//If less than firstSmall --> make secondSmall as first & firstSmall as ele
			if(ele<firstSmall) {
				secondSmall = firstSmall;
				firstSmall = ele;	
			}else if(ele<secondSmall && ele != firstSmall) {
				secondSmall = ele;
			}
		}
		
		
		//edge case if all the elements are identical in array
		if(secondSmall == Integer.MAX_VALUE) {
			System.out.println("All the elements in array are identical");
			return -1;
		}
		return secondSmall;
	}

}
