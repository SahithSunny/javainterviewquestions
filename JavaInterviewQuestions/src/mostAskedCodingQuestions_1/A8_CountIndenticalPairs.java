package mostAskedCodingQuestions_1;

import java.util.HashMap;
import java.util.Map;

/*
 * Q: Java problem to print count of Identical pairs can be formed from the elements in the given array.
 * EX: For the array {1, 1, 1, 1, 1, 2, 2}:
 * 		- The number 1 appears 5 times. We can form 2 pairs (5 // 2 = 2 pairs).
		- The number 2 appears 2 times. We can form 1 pair (2 // 2 = 1 pair).
		- The final result is the sum of these pairs: 2 + 1 = 3.
		
Approach:
1. Create a HashMap & store the frequency of each element and the frequency
2. calculate the pairs by diving each value by 2 & sum up with the final pairs count.		
 */

public class A8_CountIndenticalPairs {

	public static void main(String[] args) {
		
		int[] ary = {1, 1, 1, 1, 1, 2, 2};
		
		System.out.println("Identical pairs count :" +countPairs(ary));

	}
	
	
	 // Function to count the total number of pairs
    static int countPairs(int[] arr) {
    	int finalCountPair =0;
    	
    	//create a map to store the elements & its frequency
    	Map<Integer, Integer> hm = new HashMap<Integer,Integer>();
    	
    	for(int ele: arr) {
    		
    		if(hm.containsKey(ele)) {
    			hm.put(ele, hm.get(ele)+1);
    		}else {
    			hm.put(ele, 1);
    		}
    	}
    	//System.out.println(hm);
    	
    	//loop through the HashMap to get the value
    	for(Map.Entry<Integer,Integer> entry : hm.entrySet()) {
    		
    		//as we need to get the pairs count- we are dividing each frequency with 2 to fetch the pairs count
    		finalCountPair += entry.getValue()/2;
    	}
    	
		return finalCountPair;
    	
    }

}
