package mostAskedCodingQuestions_1;

/*
 * Q: Finding the second largest element in an array
 * eg: arr = {12, 35, 1, 10, 34, 1} - o/p: 34
 * 
 * Approach: - optimal approach using single loop
 *	1. Traverse the array while keeping track of the largest and second-largest elements.
	2. For each element, update the largest or second-largest accordingly.
	
	3. Edge cases  - if array size is <2 & if array is identical array
 * 
 */

public class A9_SecondLargestArrayEle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arr = {12, 35, 1, 10, 34, 1};
		
		System.out.println("Second largest element is : "+findSecondLargest(arr));

		
	}

	static int findSecondLargest(int[] arr) {
		
		int firstLarge = Integer.MIN_VALUE;
		int secondLarge = Integer.MIN_VALUE;
		
		if(arr.length<2){
			System.out.println("Given array 1 or Empty");
			return -1;
		}
		
		for(int ele : arr) {
			if(ele>firstLarge) {
				secondLarge = firstLarge;
				firstLarge = ele;
			}else if(ele > secondLarge && ele != firstLarge) {
				secondLarge = ele;
			}
		}
		
		if(secondLarge == Integer.MIN_VALUE) {
			System.out.println("Given array has all identical elements");
			return -1;
		}
		
		return secondLarge;
	}

}
