package stringQuestions;

/*
 What is a String?
 -----------------
Strings are considered a data type in general and are typically represented as arrays of bytes (or words) that store a sequence of characters. 
Strings are defined as an array of characters. The difference between a character array and a string is the string is terminated with a special character �\0�

Below are some examples of strings:
�geeks� , �for�, �geeks�, �GeeksforGeeks�, �Geeks for Geeks�, �123Geeks�, �@123 Geeks�
 */



public class A1_Introduction_To_Strings_DSA {
	
//How to declare Strings in Java
	public static void main(String[] args) {
		//Declare String without using new operator
		String s = "GeeksForGeeks";
		
		//Print the String 
		System.out.println("String s = "+ s);
		
		//Declare String using new operator
        String s1 = new String("GeeksforGeeks");
        
        //Prints the String.
        System.out.println("String s1 = " + s1);
        
        
	}

}
