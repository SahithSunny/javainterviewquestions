package stringQuestions;

public class RemoveConsecutiveDuplicate {

	public static void main(String[] args) {
		
		String str = "aabcca";
		System.out.println("Original string value is : "+str);
		
		String refineStr = removeDuplicates(str);
		System.out.println("refined string is : "+refineStr);

	}

	private static String removeDuplicates(String str) {
		
		if((str == null) || (str.length() <=1)){
			return "";
			
			
		}
		StringBuilder sb = new StringBuilder();
		char previousChar  = str.charAt(0);
		sb.append(previousChar);
		
		//start from second char
		for(int i=1; i<str.length(); i++) {
			char currentChar = str.charAt(i);
			
		if(currentChar 	!= previousChar)	{
			sb.append(currentChar);
		}
		previousChar = currentChar;
		
		}
		return sb.toString();
	}

}
