package stringQuestions;

/*
 * approach 1 - using loops and conditional statement
 * approach 2 - using regExp 
 * for vowels get the count - by replacing all other than 'aeiou' & get the count
 * for consonents - replace all 'aeiou' and not 'a-z' & then get the count
 */
	
public class VowelsConsnentsCount {

	public static void main(String[] args) {
		
		String str = "Hello World";
		int vowels = 0;
		int consonants = 0;
		
		//convert to lower case
		str = str.toLowerCase();
		System.out.println(str);
		
		for(int i=0; i<str.length(); i++) {
			char ch = str.charAt(i);
			
			if(ch== 'a' || ch== 'e' || ch== 'i' || ch== 'o' || ch== 'u' ) {
				vowels++;
			}else if (ch >= 'a' && ch <= 'z') {
				consonants++;
			}
		}
		
		System.out.println("Vowels count : "+vowels);
		System.out.println("Consonents count : "+consonants);
		
		
//approach 2 - using regular expressiona:		
		
		int vowels1 = str.replaceAll("[^aeiou]", "").length();

        // Count consonants using regex
        int consonants1 = str.replaceAll("[^a-z]", "") // Remove non-letters
                            .replaceAll("[aeiou]", "") // Remove vowels
                            .length();
        
        System.out.println("Vowels1 count : "+vowels1);
		System.out.println("Consonents1 count : "+consonants1);
	}

}
