package stringQuestions;
/*
 * Better approach for this question is by using the HashMap - refer the same problem in HashMap package
 */

public class FirstNonRepeatedChar {
	
	public static char FIndFirstNonRepeatedChar(String str) {
		//first convert the string into lowercase
		str = str.toLowerCase();
		int n = str.length();
		
		
		for(int i = 0; i<n; i++) {
			boolean repeated = false;
			
			for(int j = 0; j<n; j++) {
				
				if((str.charAt(i)==str.charAt(j))&& (i != j)) {
					repeated = true;
					break;
				}
			}
			
			if(!repeated) {
				return str.charAt(i);
			}
		}
		
		return 0;
		
	} 

	public static void main(String[] args) {
		
		String str = "SwissWI";
		
		char result = FIndFirstNonRepeatedChar(str);
		System.out.println("first non repeated char is : "+ result);
		

	}

}
