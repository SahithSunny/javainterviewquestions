package stringQuestions;

import java.util.Arrays;
import java.util.Collections;

public class StringAnagram {

	public static void main(String[] args) {

		String str1 = "Listen";
        String str2 = "Silemt";
        boolean resultFlag = true;
        
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();    
    
        //sort both the given arrays
        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        		
       Arrays.sort(arr1);
       Arrays.sort(arr2);
        
       
        //if - length is not equal - both strings are not anagram
        if(str1.length() == str2.length()) {
        	
        	for(int i=0; i<arr1.length; i++) {
        		if(arr1[i] != arr2[i]) {
        			resultFlag = false;
                	break;
        		}
        	}
        }else {
        	resultFlag = false;
        }
        
        
        if(resultFlag == true) {
        	System.out.println("given strings are anagram");
        }
        else {
        	System.out.println("given strings are not anagram");
        }

        
	}

}
