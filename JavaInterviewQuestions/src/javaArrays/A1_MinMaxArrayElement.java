package javaArrays;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/*
 * approach 1 - using loops & conditional statements
 * approach 2 - using collections methods
 * approach 3: using Java streams
 */

public class A1_MinMaxArrayElement {

	public static void main(String[] args) {

		// approach 1: using loops & conditional statements
		int[] arr = {15, 22, 4, 8, 19, 30, 1};
		
		int min = arr[0] , max = arr[0];
		
		for(int ele : arr) {
			if(ele < min) {
				min = ele;
			}else if(ele > max) {
				max = ele;
			}
		}
		
		System.out.println("min value :"+ min);
		System.out.println("max value :"+ max);

		
		// approach 2: using collections methods
		
		List<Integer> li = Arrays.asList(15, 22, 4, 8, 19, 30, 1);
		
		int min1 = Collections.min(li);
		System.out.println("min1 value :"+ min1);
		
		int max1 = Collections.max(li);
		System.out.println("max1 value : "+max1);
		
		
		// approach 3: using Java streams
		int min2 = Arrays.stream(arr).min().getAsInt();
		int max2 = Arrays.stream(arr).max().getAsInt();
		
		System.out.println("min2 value is :" +min2);
		System.out.println("max2 value is : "+max2);
	}

}
