package javaArrays;

import java.util.Arrays;

/*How to Print an Array without using looping concept*/

public class PrintArrayWithoutLoops {

	public static void main(String[] args) {
		
		//Get the array 
		int arr[] = {10,20,30,40};
		
		//convert array elements to string & print the string
		String stringArr = Arrays.toString(arr);
		System.out.println("The array elements are "+ stringArr);
		
		
		
		//Can also pring the array elements without creating a string	
		int data[] = {2,4,6,8,10};
		System.out.println("Array elements: "+Arrays.toString(data));

	}

}
