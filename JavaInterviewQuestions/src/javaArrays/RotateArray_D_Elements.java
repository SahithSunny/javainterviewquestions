package javaArrays;

import java.util.Arrays;

/*
 * Q; Rotate array elements to - D elements towards left
 * if Arr = {1,2,3,4,5,6,7} & D=2 - result = {3,4,5,6,7,1,2}
 * - saperate first 'D' elements from array & save in temp array
 * - rotate the remaining elements in original to D digits left
 * - at last append the temp array to original
 */

public class RotateArray_D_Elements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 int[] arr = {1, 2, 3, 4, 5, 6, 7};
		 int n = arr.length;
		 int d = 9; // 2 elements to be roatated towards left
		 
		 // if rotate count is greater than the length of the array
		 if(d>n) {
			 d=d%n;
		 }
		 
		 
		 if(d == 0 || n == 0) {
			 System.out.println("rotation not required- original array itself is roatated array");			 
		 }else {
			 
			 
			// declare a temp array of 'd' size & store first 'd' elements
			 int[] temp = new int[d];
			 
			 for(int i=0; i<d; i++) {
				 temp[i] = arr[i];
			 }
			 System.out.println("first d elements array is : "+Arrays.toString(temp));
			 
			 
			 //shift remaining elements to d- times left 
			 for(int j=0; j<n-d; j++) {
				 arr[j] = arr[d+j];
			 }
			 System.out.println("array after shifting remaining elements: "+Arrays.toString(arr));
			 
			 
			 //now append temp elements to the last of original array
			 for(int k= 0; k<d; k++) {
				 arr[n-d +k] = temp[k];
			 }
			 System.out.println("final result array after shift: "+Arrays.toString(arr));

			 
		 }

		 
	}

}
