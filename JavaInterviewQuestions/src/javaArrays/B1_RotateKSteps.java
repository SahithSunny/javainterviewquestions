package javaArrays;

import java.util.Arrays;

/*
 * Q: Rotate an Array by K Steps
	  Rotate an array to the right by k steps in-place, where k is a given non-negative integer.
	  
Approach:
	1. Reverse the whole array.
	2. Reverse the first K-elements
	3. Reverse the last 'n-k' elements.
 */

public class B1_RotateKSteps {
	
	 public static void rotate(int[] arr, int k) {
	        int n = arr.length;
	        k = k % n; // In case k > n
	        
	        // Step 1: Reverse the whole array
	        reverse(arr, 0, n - 1);
	        
	        // Step 2: Reverse the first k elements
	        reverse(arr, 0, k - 1);
	        
	        // Step 3: Reverse the last n-k elements
	        reverse(arr, k, n - 1);
	    }
	 
	 private static void reverse(int[] arr, int start, int end) {
	        while (start < end) {
	            int temp = arr[start];
	            arr[start] = arr[end];
	            arr[end] = temp;
	            start++;
	            end--;
	        }
	    }

	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int k = 3;
        rotate(arr, k);
        System.out.println(Arrays.toString(arr)); // Output: [5, 6, 7, 1, 2, 3, 4]

	}

}
