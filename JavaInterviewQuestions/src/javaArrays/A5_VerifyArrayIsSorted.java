package javaArrays;

public class A5_VerifyArrayIsSorted {
	
	public static boolean isSorted(int[] arr) {
        int start = 0;
        int end = arr.length - 1;

        while (start < end) {
            if (arr[start] > arr[start + 1]) {
                return false; // Check from start
            }
            if (arr[end] < arr[end - 1]) {
                return false; // Check from end
            }
            start++;
            end--;
        }
        return true;
    }

	public static void main(String[] args) {
		
		 int[] arr = {1, 2, 3, 4, 5};
	     System.out.println(isSorted(arr)); // Output: true

	}

}
