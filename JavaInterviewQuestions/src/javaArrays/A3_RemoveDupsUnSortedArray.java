package javaArrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;

public class A3_RemoveDupsUnSortedArray {

	public static void main(String[] args) {
		
		int[] arr = {4, 3, 2, 4, 1, 2, 3};
		
// approach 1: using HashSet		
		// add all array elements into Set- as it ony allows unique values
		HashSet<Integer> hs = new HashSet<Integer>();
		
		for(int ele: arr) {
			hs.add(ele);
		}
		
		int[] result = new int[hs.size()];
		//convert hashSet into array
		int count = 0;
		for(int ele : hs) {
			result[count++] = ele;
		}
		
		//print the array elelemts
		System.out.println("original array is : "+Arrays.toString(arr));
		System.out.println("after eleminating the dups array is : "+Arrays.toString(result));
		
// approach 2: using java streams
		
		int[] result1 = Arrays.stream(arr).distinct().sorted().toArray();  // use 'sorted()' incase we need results as sorted way
		
		//print the array elelemts
		System.out.println("original array is : "+Arrays.toString(arr));
		System.out.println("result array using streams is : "+Arrays.toString(result1));
		

	}

}
