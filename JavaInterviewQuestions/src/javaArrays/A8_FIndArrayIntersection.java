package javaArrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Q: Find the Intersection of Two Arrays
	  Given two arrays, write a program to find their intersection.
approach: 	 
     First, sort both arrays and then use two pointers to traverse both arrays and find the intersection.
 */

public class A8_FIndArrayIntersection {
	
	public static List<Integer> findIntersection(int[] arr1, int[] arr2) {
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        List<Integer> intersection = new ArrayList<>();
        int i = 0, j = 0;

        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] == arr2[j]) {
                intersection.add(arr1[i]);
                i++;
                j++;
            } else if (arr1[i] < arr2[j]) {
                i++;
            } else {
                j++;
            }
        }

        return intersection;
    }

	public static void main(String[] args) {
		int[] arr1 = {1, 3, 4, 2};
        int[] arr2 = {2, 1};
        List<Integer> result = findIntersection(arr1, arr2);
        System.out.println(result); // Output: [2, 2]

	}

}
