package javaArrays;

/*
WAP to print the smallest number & find the largest number in that column
-> Find the smallest number in the matrix
-> find the column index of that smalest number present
-> print the largest number from that column
*/

public class MinNum_MaxNumInColumn {

	public static void main(String[] args) {


		int arr[][]= {{2,5,4},{7,2,6},{4,7,1}};
		int temp= arr[0][0];
		int mincolumn = 0;
		
		for(int i=0; i<3; i++)
		{
			for (int j=0; j<3; j++)
			{
				if(temp > arr[i][j])
				{
					temp = arr[i][j];
					mincolumn=j;
				}
			}
		}
		
		System.out.println("the minimum number is "+temp);
		
		// Store the min column of the 
		int max = arr[0][mincolumn];
		int k = 0;
		while(k<3)
		{
			if(arr[k][mincolumn] > max )
			{
				max = arr[k][mincolumn];
			}
			
			k++;
		}
		
		System.out.println("The maximum number is "+max);

	}

}
