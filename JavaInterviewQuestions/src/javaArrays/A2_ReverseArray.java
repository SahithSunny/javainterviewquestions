package javaArrays;

public class A2_ReverseArray {

	public static void main(String[] args) {
		
		int[] arr = {4, 15, 1, 2, 0, 4, 12, 5};
		
		for(int i=arr.length-1; i>=0; i-- ) {
			System.out.print(arr[i]+" ");
		}
		
 // approach 2 - using 2 pointer
		System.out.println("");
		System.out.println("------------approach 2 --------------");
		int[] arr1 = {1, 2, 3, 4, 5};
		int left = 0;
        int right = arr1.length - 1;

        while (left < right) {
            // Swap elements
            int temp = arr1[left];
            arr1[left] = arr1[right];
            arr1[right] = temp;

            left++;
            right--;
        }
        
        for (int num : arr1) {
            System.out.print(num + " "); // Output: 5 4 3 2 1
        }

	}

}
