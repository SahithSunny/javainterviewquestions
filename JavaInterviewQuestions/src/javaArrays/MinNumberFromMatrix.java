package javaArrays;

//WAP to print the minimum/smallest number from the given 3*3 matrix

// Print all the values in the given matrix, compare the all remaining values with the first value in the matrix 

public class MinNumberFromMatrix {

	public static void main(String[] args) {

		int arr[][] = { { 2, 5, 6 }, { 7, 13, 1 }, { 6, 1, 11 } };
		int min = arr[0][0];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				// compaing each value with first value in the array
				if (arr[i][j] < min) {
					min = arr[i][j];
				}
			}
		}
		System.out.println("The minimum value in the Given matrix is " + min);
	}
}
