package javaArrays;

/*
 * Q: Find All Pairs in an Array with a Given Sum
	  Given an array of integers and a target sum, find all pairs of integers in the array whose sum equals the target.
	  
Approach: using HashSet
This approach ensures that each pair is unique by storing the complements in a HashSet. It also avoids using the same element twice.	  
 */

import java.util.HashSet;

public class A6_ArrayPairsSum {
	
	    public static void findPairs(int[] arr, int target) {
	        HashSet<Integer> set = new HashSet<>();

	        for (int num : arr) {
	            int complement = target - num;
	            if (set.contains(complement)) {
	                System.out.println("(" + complement + ", " + num + ")");
	            }
	            set.add(num); // Add the current number to the set
	        }
	    }

	public static void main(String[] args) {
		int[] arr = {2, 7, 11, 15, -2, 6};
        int target = 9;
        findPairs(arr, target); // Output: (7, 2), (11, -2)

	}

}
