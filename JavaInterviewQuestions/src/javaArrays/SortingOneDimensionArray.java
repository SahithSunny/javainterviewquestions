package javaArrays;

/*WAP to sort the given one dimension array to ascending order
-> compare the first element of array with all the other elements
-> if any small number then first number found in the array - then swap
-> retrive all the array elements using for loop & print the sorted array*/

public class SortingOneDimensionArray {

	public static void main(String[] args) {
		
		int arr[]= {4,8,2,6,3,7};
		int temp;
		
		
		for(int i=0; i<arr.length; i++)
		{
			for(int j=i+1; j<arr.length; j++)
			{
				if(arr[i] > arr[j])
				{
					//swap
					temp = arr[i];
					arr[i] = arr[j];
					arr[j]= temp;		
				}
			}
			
		}
		
		// retriving the sorted array elements
		for(int i=0; i<arr.length; i++ )
		{
			System.out.print(arr[i]+",");
		}

	}

}
