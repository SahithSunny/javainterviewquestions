package javaArrays;

/*
 * Sum�of�numbers�from�1�to�N= N�(N+1)/2
 * 
 */
public class A9_MissingNumber1toN {
	
	public static void main(String args[]) {
		
		 int[] arr = {1, 2, 4, 5, 6};
		 
		 int n = arr.length+1; // as a number is missing in the given result
		 int expSum = n*(n+1)/2;
		 int actSum = 0;
		 
		 //find the sum of elements in given array
		 for(int ele : arr) {
		 actSum = actSum +ele;	 
		 }
		 
		 int missingElement = expSum - actSum;
		 System.out.println("missing element in given array is : "+ missingElement);
		 
		
	}

}
