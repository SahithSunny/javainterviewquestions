package javaArrays;

/*Find the second largest number in the given array
-> Sort the given array in Decending order.
-> So the first index number will be the max & second index number will be second max in the array.
*/

public class A4_SecondMaxNumInMatrix {

	public static void main(String[] args) {

		int Array[] = { 1, 6, 7, 2, 4, 5, 8 };
		int temp = 0;

		for (int i = 0; i < Array.length; i++) {
			for (int j = i + 1; j < Array.length; j++) {
				if (Array[i] < Array[j]) {
					temp = Array[i];
					Array[i] = Array[j];
					Array[j] = temp;
				}
			}
		}

		for (int i = 0; i < Array.length; i++) {
			System.out.print(Array[i] + ",");

		}
		System.out.println();

		System.out.println("The second largest number in the array is " + Array[1]);

	}

}
