package javaArrays;

import java.util.ArrayList;
import java.util.List;

public class MissingNumbsArray {
	
	
	  public static List<Integer> findMissingUsingBooleanArray(int[] arr, int size) {
	  
	        boolean[] present = new boolean[size + 1]; // Array to mark elements as present (1 to size)
	        
	        // Mark elements that are present in the input array
	        for (int num : arr) {
	            if (num <= size) {
	                present[num] = true;
	            }
	        }
	        List<Integer> missing = new ArrayList<Integer>();
	        // Collect missing elements
	        for (int i = 1; i <= size; i++) {
	            if (!present[i]) {
	                missing.add(i);
	            }
	        }
	        return missing;
	  }

	public static void main(String[] args) {
		 int[] arr = {1, 3, 5, 7, 9, 8, 4};
	        int size = 9;
	        
	        List<Integer> missing = findMissingUsingBooleanArray(arr, size);
	        System.out.println("Missing elements: " + missing); // Output: [2, 6]

	}

}
