package java_30_questions_pavan;

import java.util.Scanner;

public class Reverse_a_number_2 {
	
	// We will have a look at 3 ways to reverse a number

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int num = sc.nextInt(); //1234
		
		//METHOD 1 - Using algorithm
		int rev=0;
		
		while(num!=0) {
			rev=rev*10 + num%10; //0 + 1234%10 = 4 -> 40+3 = 43 -> 430+2 = 432 -> 4320+1=4321 
			num=num/10;         // 1234/10 = 123   -> 123/10=12 -> 12/10 = 1   -> 1/10=0
		}
		
		System.out.println("Reverse Number is = "+rev);
		
		
		
		//METHOD 2 - Using String buffer method
		System.out.println("Enter a second Number");
		int num2 = sc.nextInt(); //4567
		StringBuffer rev2;
		
		StringBuffer sb = new StringBuffer(String.valueOf(num2));
		rev2 = sb.reverse();
				
		System.out.println("Reverse Number is = "+rev2);
		
		
		
		//METHOD 3 - Using String builder method
		System.out.println("Enter a third Number");
		int num3 = sc.nextInt(); //7890
		StringBuilder rev3;
		
		StringBuilder sb1 = new StringBuilder();
		sb1.append(num3);
		rev3 = sb1.reverse();
				
		System.out.println("Reverse Number is = "+rev3);
		
	}

}
