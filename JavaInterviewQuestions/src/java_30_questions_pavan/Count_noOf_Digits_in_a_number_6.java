package java_30_questions_pavan;

public class Count_noOf_Digits_in_a_number_6 {
	
	// Should return the no.of Digits available in a given number eg:145678 - count=6

	public static void main(String[] args) {
		
		//METHOD 1 - convert into string & get length
		int num = 145678;
		int noOfDigits = 0;
		
		String numbToStr = num +"";
		noOfDigits = numbToStr.length();
		
		System.out.println("no.of Digits available in a given number "+num+" = "+noOfDigits);
		
		
		//METHOD 2 - using String.valueOf() function
		int num1 = 123456;
		System.out.println("no.of Digits available in a given number = "+String.valueOf(num1).length());
		
		
		//METHOD 3 - using algorithm- divide num by 10 untill num = 0 & add counter for each loop
		int numb = 123456;
		int count = 0;
		
		while(numb>0) {
			numb = numb/10;
			count++;
		}
		
		System.out.println("count of no.of digits = "+count);
		
	}

}
