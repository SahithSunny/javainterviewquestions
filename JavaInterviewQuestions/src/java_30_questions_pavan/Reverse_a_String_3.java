package java_30_questions_pavan;

public class Reverse_a_String_3 {
	
	//3 ways to reverse a String

	public static void main(String[] args) {
		
		//METHOD 1 - using string concatenation function
		// str -> loop till length -> loop through reverse - charAt[Str] -> concat to revString
		
		String str = "ABCDE";
		System.out.println("Actual str = "+str);
		String reverse = "";
		
		int len = str.length();
		
		for(int i=len-1; i>=0; i--) {
			reverse = reverse+ str.charAt(i);
		}
		
		System.out.println("Reverse of str = "+reverse);
		
		
		//METHOD 2 - Using character array
		// save the string in char array- toCharArray method & loop reverse & print rev array
		
		String str2 = "EFGH";
		System.out.println("Actual str2 = "+str2);
		String reverse2 = "";
		
		char a[]= str2.toCharArray();
		
		for(int i=a.length-1; i>=0; i--) {
			reverse2 = reverse2+a[i];
		}		
		
		System.out.println("Reverse of str2 = "+reverse2);
		
		
		//METHOD 3 - Using String Buffer class
		
		String str3 = "I_LOVE_YOU";
		System.out.println("Actual str3 = "+str3);
		String reverse3;
		
		StringBuffer sb = new StringBuffer(str3);
		reverse3 = sb.reverse().toString();
		
		System.out.println("Reverse od str3 = "+reverse3);


	}

}
