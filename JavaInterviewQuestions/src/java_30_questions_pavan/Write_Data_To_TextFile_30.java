package java_30_questions_pavan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Write_Data_To_TextFile_30 {
	/*
	 * We will be using 2 classes FileWriter & BufferWriter
	 */

	public static void main(String[] args) throws IOException {
		
		FileWriter fw = new FileWriter("C:\\Users\\SAHITH_KUMAR_P\\Downloads\\TempWriter.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("Selenium with Java");
		bw.write("\n");
		bw.write("Selenium with C#");
		bw.write("\n");
		bw.write("Selenium with Python");
		
		System.out.println("Finished!!!!");
		
		bw.close();
		fw.close();

	}

}
