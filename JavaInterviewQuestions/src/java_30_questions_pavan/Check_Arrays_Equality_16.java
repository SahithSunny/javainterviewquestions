package java_30_questions_pavan;

import java.util.Arrays;

public class Check_Arrays_Equality_16 {

	public static void main(String[] args) {
		//METHOD 1 - using the inbuild 'equals' method.
		
		int[] A = {1,2,3,4,5};
		int[] B = {1,2,3,7,5};
		
		boolean status = Arrays.equals(A, B);
		
		//ternary operator for comparison - instead of if-else
		String result = (status == true) ? "Arrays are Equal": "Arrays are not Equal";
		
		System.out.println(result);
		
		//METHOD 2 - without using the equals - pre defined method
		
		// approach - first check the count of 2 arrays 
		boolean finalStatus = true;
		
		if(A.length != B.length) {
			finalStatus = false;
		}else {
			
			for(int i= 0 ; i<A.length; i++) {
				
				if(A[i] != B[i]) {
					finalStatus = false;
					break;    // if any one of the element is not match, we can confirm - not equal & can break the loop
				}
			}
		}
		
		if(finalStatus == true) {
			System.out.println("Both the Arrays are Equal");
		}else {

			System.out.println("Both the Arrays are NOT Equal");
		}
		
	}

}
