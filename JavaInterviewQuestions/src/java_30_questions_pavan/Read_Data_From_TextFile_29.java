package java_30_questions_pavan;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Read_Data_From_TextFile_29 {
	/*
	 * approach 1- using fileReader & BufferedReader classes
	 * approach 2- using File & Scanner class
	 */

	public static void main(String[] args) throws IOException {

// Approach 1 - using the FileReader & BufferedReader classes
		
		FileReader fr = new FileReader("C:\\Users\\SAHITH_KUMAR_P\\Downloads\\PLSjobLaunchText.txt");
		BufferedReader br = new BufferedReader(fr);
		
		String str;
		
		while((str = br.readLine())!= null) {
			System.out.println(str);
		}
		
		br.close();
		fr.close();
		
//Approach 2 - using the FIle and the Scanner class
		
		System.out.println("");
		
		File file = new File("C:\\Users\\SAHITH_KUMAR_P\\Downloads\\PRD.txt");
		
		Scanner sc = new Scanner(file);
		while(sc.hasNextLine()) {
			System.out.println(sc.nextLine());
		}
		

	}

}
