package java_30_questions_pavan;

import java.util.Random;

//import org.apache.commons.lang3.RandomStringUtils;

public class Generate_Random_Numbers_Strings_12 {

	public static void main(String[] args) {

		//METHOD 1 - Using Random built in function
		
		Random ran = new Random();
		int random_numb = ran.nextInt(100);  //range random numb should be B/w this range- max=9999
		System.out.println(random_numb);
		
		double random_double = ran.nextDouble(); //range = 0.0 to 1.0
		System.out.println(random_double);
		
		
		//METHOD 2 - Using Math class- we can generate only decimal - later than we can convert double to int- using type cast
		
		System.out.println("random Math = "+Math.random());
		
		//METHOD 3 - Apache commons- lang API - o generate random numbers & strings
		//Navigate to https://commons.apache.org/proper/commons-lang/download_lang.cgi -> download this zip - commons-lang3-3.16.0-bin.zip
		//Eclipse -> project -> properties -> java build path -> libraries -> external jars -> add ext jars -> select & add 'commons-lang3-3..' jar -> Apply save
		// now we should see the jar - in project under - Referenced libraries
		
/*
		//In below code "RandomStringUtils" function will through error- if the above jar is not setup in external libraries- follow above steps
		String randomNum = RandomStringUtils.randomNumeric(5);  // a random number will gets generated of 5 digits max = 99999 - return type = STring
		System.out.println("random numeric 5 digits = "+randomNum);
		
		String randomStr = RandomStringUtils.randomAlphabetic(5);
		System.out.println("random string 5 digits = "+randomStr);
		
		String randaomAplhaNum = RandomStringUtils.randomAlphanumeric(6);
		System.out.println("random aplha numeric 6 digits = "+randaomAplhaNum);
	 	
*/

	}

}
