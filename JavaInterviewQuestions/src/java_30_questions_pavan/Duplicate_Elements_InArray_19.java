package java_30_questions_pavan;

import java.util.HashSet;

public class Duplicate_Elements_InArray_19 {

	public static void main(String[] args) {
		
		String arr[] = {"java","python","cypress","typescript","java"};
		boolean dupFlag = true;
		
		//Approach 1 - to pick one element & compare with the rest of array variables
		for(int i=0; i<arr.length; i++) {
			for(int j=i+1; j<arr.length; j++) {
				
				if(arr[i]== arr[j]) {
					dupFlag = false;
					System.out.println("Found Duplicate value in given array");
					break;
				}
			}
		}
		
		if(dupFlag == true) {
			System.out.println("No duplicate value found in given array");
		}
		
		
		//Approach 2 - using HashSet - compare the actual array size & the setSize
		
		HashSet<String> hs = new HashSet<String>();
		int originalArrSize = arr.length;
		//System.out.println("original arr length = "+originalArrSize);
		
		for(String str: arr) {
			hs.add(str);
		}
		
		int setSize = hs.size();	
		//System.out.println("original arr length = "+setSize);
		
		//compare the actual array size & the set size- if both are equal - no duplicates - else we have duplicates
		if(originalArrSize == setSize) {
			System.out.println("No Duplicate value in given array- method 2");
		}else {
			System.out.println("Found Duplicate value in given array - method 2");
		}
		

	}

}
