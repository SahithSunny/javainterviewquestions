package java_30_questions_pavan;

public class Linear_search_In_Array_20 {
	/*
	 * Given a Array of elements & a Key value, we need to verify if any of the array Elements are matching with the Given Key.
	 * EG: int[] array = {10,20,40,30,50}- key = 50
	 * Search through the whole array elements & know if there is any element matches with key.
	 * LEANEAR SEARCH / SEQUENTIAL SEARCH
	 */

	public static void main(String[] args) {
		int[] arr = {10,20,30,50,40};
		
		int searchKey = 50;
		boolean flag = false;
		
		for(int i=0; i<arr.length;i++ ) {
			if(searchKey == arr[i]) {
				flag = true;
				System.out.println("Element is found");
				break;
			}
		}
		
		if(flag == false) {
			System.out.println("Element not Found");
		}
		

	}

}
