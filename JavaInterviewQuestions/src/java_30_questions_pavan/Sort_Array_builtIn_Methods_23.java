package java_30_questions_pavan;

import java.text.CollationElementIterator;
import java.util.Arrays;
import java.util.Collections;

public class Sort_Array_builtIn_Methods_23 {
	
	/*
	 * We will look at the different built-in methods to sort the Array
	 */
	
	public static void main(String[] args) {
		
// approach 1 - using parallel sort method
		
		int[] arr1 = {30,50,20,10,60};
		
		System.out.println("array elements- arr1 - before sorting: "+Arrays.toString(arr1));
		
		Arrays.parallelSort(arr1);
		System.out.println("array elements after parallel sort: "+ Arrays.toString(arr1));
		
// approach 2 - using sort method
		int[] arr2 = {-10,5,0,-5,10};
		
		System.out.println("array elements- arr2 - before sorting: "+Arrays.toString(arr2));
		
		Arrays.sort(arr2);
		System.out.println("array elements after sort: "+ Arrays.toString(arr2));
		
// Sort array elements in decending order
		
		Integer[] arr3 = {-10,5,0,-5,10};
		Arrays.sort(arr3, Collections.reverseOrder()); 
		// 'Collections.reverseOrder()' will only support primitive datatypes - so we declared array with 'Integer' class
		System.out.println("array elements - arr3 - in decending order: "+ Arrays.toString(arr3));
		
		
	}

}
