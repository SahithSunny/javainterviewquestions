package java_30_questions_pavan;

import java.util.Scanner;

public class Count_words_in_string_27 {
	/*
	 * given a String value - we need to return the count of the words present in the given string
	 * approach: we will declare a counter flag & initial value as 1 -> then loop through each character & 
	 * if (character at i = ' ' && character at i+1 is not null) -> then increase the count to +1
	 */

	public static void main(String[] args) {
		
		System.out.println("Enter a String value");
		Scanner scr = new Scanner(System.in);
		String str = scr.nextLine();
		
		int counter = 1;
		
		for(int i=0; i<str.length()-1; i++) {
			
			if((str.charAt(i) == ' ') && (str.charAt(i+1) != ' ')) {
				counter++;
			}
		}
		
		System.out.println("The number words in the given string is: "+counter);

	}

}
