package java_30_questions_pavan;

import java.util.Scanner;

public class EvenCount_OddCount_Digits_given_number_7 {
	
	//eg: numb = 145678 - even count = 3, odd count = 3
    //Extract last number by % - if that numb%2 == 0 -> evenCount++ , else oddCount++
	public static void main(String[] args) {
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter a number:");
		
		int num = scr.nextInt();
		int even_count=0;
		int odd_count=0;
		
		while(num>0) {
			int reminder = num%10;
			
			if(reminder%2==0) {
				even_count++;
			}else {
				odd_count++;
			}
			
			num = num/10;
		}
		
		System.out.println("Even digits count = "+even_count);
		System.out.println("Odd digits count = "+odd_count);

	}

}
