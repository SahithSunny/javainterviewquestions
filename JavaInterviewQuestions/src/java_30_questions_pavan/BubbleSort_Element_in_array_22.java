package java_30_questions_pavan;

import java.util.Arrays;

public class BubbleSort_Element_in_array_22 {
	
	/*
	 * In bubble sort - we have to sort the array in ascending order
	 * suppose array is int[] a = {4,2,1,5,3};
	 * as length = 5, we need to loop 'length-1' times this will be the outer loop
	 * inside we need to again loop 'length -1' times 
	 * comparing the each index - 0th index is greater than 1st index -> yes - swap 2 elements -> no - do not enter swap block
	 * by end of first outer loop - highest value index will be set to last index -> {2,1,4,3,5}
	 * simillarly 2nd outer loop - last 2 elements sorted -> {1,2,3,4,5}
	 * 3rd outer loop last 3 elements - gets sorted -> {1,2,3,4,5}
	 * last 4th loop last 4 elements gets sorted & lowest value gets to 0th index -> {1,2,3,4,5}
	 * 
	 * total 'length of array - 1' loops both outer & inner
	 * 
	 */

	public static void main(String[] args) {
		
		int[] arr = {22,-11,90,27,8,63,18,-33};
		
		System.out.println("Array before sorting : "+ Arrays.toString(arr));
		
		int n = arr.length;
		
		for(int i=0; i<n-1; i++) { //outer loop for each element sorting
			
			for(int j=0; j<n-1; j++) { //inner loop in each sorting  - swap iterations
				
				if(arr[j] > arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		
		
		System.out.println("Array after the bubble sort: "+Arrays.toString(arr));
		
		

	}

}
