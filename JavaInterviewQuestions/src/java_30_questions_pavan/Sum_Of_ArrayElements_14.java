package java_30_questions_pavan;

public class Sum_Of_ArrayElements_14 {
	
	/*
	 *If a Array is [5,2,7,5,8] - we need a write a logic to traverse through array & sum each value
	 *print the sum at end =  27 
	 */

	public static void main(String[] args) {
     
		int A[] = {5,2,7,5,8};
		int sum = 0;
		
//		for(int i=0; i<A.length; i++ ) {
//			sum = sum+A[i];
//		}
		
		//METHOD 2 - Using Enhanced for loops OR For each loop
		for( int val : A) {
			sum = sum+val;
		}
		
		System.out.println("Sum of above array elements = "+sum);
		
		
		

	}

}
