package java_30_questions_pavan;

import java.util.Scanner;

public class Sumof_Digits_given_number_8 {
	
	//to get the last digit of numb = %10, to eliminate last digit /10 -> add new digit to sum & at end of while loop print sum
	// eg; numb = 4444 - count =16

	public static void main(String[] args) {
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter any Int vaue:");
		
		int numb = scr.nextInt();
		int originalNum = numb;
		int sum = 0;
		
		while(numb>0) {
			int a = numb%10;
			sum=sum+a;
			
			numb=numb/10;
		}

		System.out.println("sum of digits in given number - "+originalNum+" = "+sum);
	}

}
