package java_30_questions_pavan;

public class Generate_Fibonacci_series_10 {
	
	/*
	 * Fibonacci numbers = a series of numbers in which each number is sum of the preceding numbers.
	 * eg: 0 1 1 2 3 5 8 13
	 * 
	 * LOGIC = take n1 & n2 as fixed values 0 & 1 - start loop from 2 to 10/ any number
	 * add n1+n2 and assign to sum, n1 = n2, n2 = sum
	 * 
	 * PRINT FIRST 10 FIBONACCI NUMBERS
	 */

	public static void main(String[] args) {
		
		int n1=0, n2=1, sum = 0;
		
		System.out.print(n1+" "+n2);
		
		for(int i=3; i<=10; i++) {
			
			sum = n1+n2;
			System.out.print(" "+sum);
			n1=n2;
			n2=sum;
		}
		

	}

}
