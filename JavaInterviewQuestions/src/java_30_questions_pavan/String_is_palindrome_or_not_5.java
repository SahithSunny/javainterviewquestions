package java_30_questions_pavan;

import java.util.Scanner;

public class String_is_palindrome_or_not_5 {
	
	/*
	 In previous lectures, we have seen how to reverse any string
	 We need to use any one approach & compare the original with rev String
	 if original str == reverse string -- given STring is Palindrome else not a Palindrome
	 */

	public static void main(String[] args) {
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter a String: ");
		
		String str = scr.nextLine();
		String originalStr = str;
		
		String rev = "";
		
		int len = str.length();
		for(int i=len-1; i>=0; i--){
			rev = rev+str.charAt(i);
		}
		
		//System.out.println("Reverse String: "+rev);
		
		if(originalStr.equals(rev)) {
			System.out.println("Given String- "+ originalStr +" -is a palindrome");
		}else {
			System.out.println("Given String- "+ originalStr +" -is not Palindrome");
		}
		

	}

}
