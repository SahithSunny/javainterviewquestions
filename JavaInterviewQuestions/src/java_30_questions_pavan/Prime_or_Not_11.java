package java_30_questions_pavan;

import java.util.Scanner;

public class Prime_or_Not_11 {
	
	/*
	 * 1. Natural numbers > 1
	 * 2. Which has only 2 factors 1 & itself is called as PRIME numbers
	 * eg: 1 & 19 - 19 is prime number
	 * 28 - 1,2,4,7,14,28 - 28 is Not a prime number
	 * prime examples are 2,3,5,7,11,13,17
	 */

	public static void main(String[] args) {
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter a Number:");
		
		int numb = scr.nextInt();
		int count = 0;
		
		if(numb>1) {
			for(int i=2; i<=numb; i++ ) {
				if(numb%i == 0) {
					count++;
				}
			}
			
			if(count == 1) {
				System.out.println("Given Number is Prime number");
			}else {
				System.out.println("Not a Prime Number");
			}
			
		}else {
			System.out.println("Not a Prime Number");
		}
		

	}

}
