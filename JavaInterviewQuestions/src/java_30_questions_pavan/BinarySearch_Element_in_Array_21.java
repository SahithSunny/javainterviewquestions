package java_30_questions_pavan;

import java.util.Arrays;

public class BinarySearch_Element_in_Array_21 {
	/* Given array int A[] = {10,20,30,40,50,60,70,80,90,100}; - sorted array
	 * FOrmula to find mid of array = (L+H)/2- IF value is in decimals- capture the floor value. - (0+9)/2 = 4- 4th index is mid
	 * A Key is given - this is searchable element
	 * CASE1: if searchable element is < mid value - high = mid - 1
	 * CASE2: If searchable element is > mid value - low = mid +1
	 * CASE3: If searchable is mid element - directly return the mid index
	 * 
	 * Here repeat the above loop and divide the array, based on above 3 cases- untill CASE1 matches key==MID value
	 * MAIN CONCEPT OF THE BINARY SEARCH IS TO SPLIT THE ARRAY BASED ON MID VALUE - UNTILL MID= KEY- Prequisit is array should be sorted order
	 */
	
	

	public static void main(String[] args) {
		
		
// Approach 1 - using the logic
		int[] arr = {1,2,3,4,5,6,7,8,9};
		boolean flag = false;
		
		int key = 6;
		int low = 0;
		
		int high = arr.length-1;
		
		while(low<=high) {
			
			int mid = (int)(low+high)/2;
			
			if(arr[mid]== key) {
				System.out.println("Element found at - "+mid+" index");
				flag = true;
				break;
			}
			
			if(arr[mid]< key) {
				low = mid + 1;
			}
			
			if(arr[mid]> key) {
				high = mid-1;
			}
		}
		
		if(flag == false) {
			System.out.println("Element is not found");
		}
		
		
		
// approach 2 - using the built in function 'binary search'
		
		int[] arr2 = {10,20,30,40,50,60,70};
		
		System.out.println(Arrays.binarySearch(arr2, 80)); 
		
		/*
		 * Above will return the index of key - if the element is present.
		 * returns some negative value- if key not present
		 */
		
		

	}

}
