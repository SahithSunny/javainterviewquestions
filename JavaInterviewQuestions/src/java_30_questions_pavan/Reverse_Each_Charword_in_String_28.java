	package java_30_questions_pavan;

public class Reverse_Each_Charword_in_String_28 {
	
	/*
	 * Q; Given a String - we need to print the reverse of each character word in the string
	 * EG: Original str = WELCOME TO JAVA -> o/p= EMOCLEW OT AVAJ
	 * first split the string into string arrays based on the whitespace -> reverse the each array - finally add all reverse arrays into single string
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str = "WELCOME TO JAVA";
		System.out.println("Original string is: "+str);
		
		String[] arr = str.split(" ");
		String revString = "";
		
		for(String word: arr) {
			String reverseWord = "";
			
			for(int i=word.length()-1; i>=0; i--) {	
				reverseWord = reverseWord + word.charAt(i);
			}
			
			revString = revString + reverseWord+ " ";
		}
		
		System.out.println("The reverse string value is: "+revString);
		
		
//Approach 2 - using the stringBuilder - stringReverse in build method
		
		String str2 = "Im almost done with everything!!!";
		System.out.println("original 2nd approach string is: "+str2);
		
		String[] words = str2.split("\\s");  // we used regEx for whitespace
		StringBuilder rSb = new StringBuilder();
		
		for(String w: words) {
			StringBuilder sb = new StringBuilder(w);
			sb.reverse();
			
			rSb.append(sb).append(" ");
		}
		
		System.out.println("reverse string - approach 2 is: "+rSb.toString());
		
		
		
	}
	
	

	
	

}
