package java_30_questions_pavan;

import java.util.Scanner;

public class Factorial_of_a_number_13 {

	/*
	 * Factorial - is a function that multiplies a number by every number below it
	 * eg: 5! = 5*4*3*2*1 = 120  OR 5! = 1*2*3*4*5 = 120
	 */
	public static void main(String[] args) {
		
		Scanner scr = new Scanner(System.in);
		System.out.println("Enter a Number: ");
		
		int numb = scr.nextInt();
		int fact=1;
		
		for(int i=1; i<=numb; i++) {
			
			fact = fact*i;
		}
		
		System.out.println("Factorial of given number is = "+fact);

	}

}
