package java_30_questions_pavan;

public class SwapTwoNumbers_1 {
	
	/* We will look at 5 ways to swap 2 numbers */

	public static void main(String[] args) {
		
		//METHOD 1 - Using third variable
		int a = 10; int b = 20;
		
		System.out.println("Before Swapping values are -> a = "+a+", b = "+b);
		
		int t=a;
		a=b;
		b=t;
		
		System.out.println("After Swapping values are -> a = "+a+", b = "+b);
		
		
		//METHOD 2 - without using third variable addition & substraction
		int a1 = 10; int b1 = 20;
		
		System.out.println("Before Swapping values are -> a1 = "+a1+", b1 = "+b1);
		
		a1 = a1+b1; //a=10+20 = 30
		b1 = a1-b1; //30-20 = 10
		a1 = a1-b1; // 30-10 = 20
		
		
		System.out.println("After Swapping values are -> a1 = "+a1+", b1 = "+b1);
		
		//METHOD 3 - using multiplication & division - only applicable if a2 & b2 are non zeros
		int a2=10; int b2=20;
		
		System.out.println("Before Swapping values are -> a2 = "+a2+", b2 = "+b2);

		a2 = a2*b2; //10*20 = 200 
		b2 = a2/b2; //200/20 = 10
		a2 = a2/b2; //200/10 = 20
		
		System.out.println("After Swapping values are -> a2 = "+a2+", b2 = "+b2);
		
		//METHOD 4 - Using bitwise XOR operator (^ operator)
		int a3 = 10; int b3 = 20;
		
		System.out.println("Before Swapping values are -> a3 = "+a3+", b3 = "+b3);
		
		//as per the XOR table if both are 0,0 or 1,1 = 0 -- 0,1 or 1,0 = 1
		a3 = a3^b3; // 10^20 = 30
		b3 = a3^b3; // 30^20 = 10
		a3 = a3^b3; // 30^10 = 20
		
		System.out.println("After Swapping values are -> a3 = "+a3+", b3 = "+b3);
		
		
		//METHOD 5 using single statement
		int a4 = 10; int b4 = 20;
		System.out.println("Before Swapping values are -> a4 = "+a4+", b4 = "+b4);
		
		b4=a4+b4-(a4=b4);
		
		//exe from right to left (a4=b4) --> a4=20, a4+b4 = 30,  b4 = 30-(20) =10
		
		System.out.println("After Swapping values are -> a4 = "+a4+", b4 = "+b4);
		

	}

}
