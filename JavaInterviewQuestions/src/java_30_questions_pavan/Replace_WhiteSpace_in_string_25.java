package java_30_questions_pavan;

public class Replace_WhiteSpace_in_string_25 {
	/*
	 * We can remove the white spaces in a string using - replaceAll builtin function & regex
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str  ="This Is Java  Coding   Practice";
		System.out.println("Actual string with white spaces: "+str);
		
		String refineStr = str.replaceAll("\\s", "");  //  double backslash s - indicates the white spaces in regex
		System.out.println("Refined String Value is : "+ refineStr);

	}

}
