package java_30_questions_pavan;

public class Print_Even_Odd_numbers_FromArray_15 {
	

	public static void main(String[] args) {
		
		int A[] = {3,6,8,47,90,-4,-9,-29,0};
		
		System.out.println("Even numbers from above array is: ");
		for(int val : A) {
			if(val%2 == 0) {
				System.out.print(val+",");
			}
		}
		System.out.println("");
		
		
		System.out.println("Odd numbers from above array is: ");
		for(int val : A) {
			if(val%2 != 0) {
				System.out.print(val+",");
			}
		}
		
		
	}

}
