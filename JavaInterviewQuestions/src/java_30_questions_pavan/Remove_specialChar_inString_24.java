package java_30_questions_pavan;

public class Remove_specialChar_inString_24 {
	/*
	 * We can get rid of any special chars in a string using the regex java methods and the replace method
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String value = "Train #42 Whistle @7 Quick *Fox +Dogs 2024";
		System.out.println("Actual String value is : "+ value);
		
		String refineValue = value.replaceAll("[^a-z A-Z 0-9 ]", "");  //replace characters other than a-z, A-Z, 0-9 and whte space - replace with empty
		System.out.println("Refined String value is : "+ refineValue);
		
	}

}
