package java_30_questions_pavan;

import java.util.Scanner;

public class Number_is_palindrome_or_not_4 {
	
	//Palindrome is a number which remains same- even if its reversed eg: 16461

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		
		int num = sc.nextInt();
		int org_num = num;
		int rev =0;
		
		while(num!=0) {
			rev= rev*10 + num%10;
			num=num/10;
		}
		
		System.out.println(rev);
		
		if(org_num == rev) {
			System.out.println("Palindrome Number");
		}else {
			System.out.println("Not Palindrome Number");
		}
		
	}

}
