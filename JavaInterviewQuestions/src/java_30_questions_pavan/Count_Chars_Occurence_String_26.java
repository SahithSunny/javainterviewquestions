package java_30_questions_pavan;

public class Count_Chars_Occurence_String_26 {
	/*
	 * Given a string and asked to find the occurance of each char in the whole string
	 * first get the string length -> next replace all that char values with null and then grab the length
	 * Diff will be the occurance of that particular character
	 */

	public static void main(String[] args) {
		
		String str = "Java Programming Java Oops";
		int totalCount = str.length();
		
		String replaceString = str.replace("r", "");
		int count_afterRemove = replaceString.length();
		
		System.out.println("Count of 'r' occurance in above string is: "+ (totalCount - count_afterRemove) );
		
	}

}
