package java_30_questions_pavan;

import java.util.Arrays;

public class Find_missing_number_in_Array_17 {
	/*
	 * missing numbers in a sequence
	 * 1. Array should not contain any duplicate values
	 * 2. Array no need to be in sorted order
	 * 3. Values should be in proper range - which has a start & end value
	 * eg: int[] A = {2,3,1,6,4,8,7}  -- here range from 1-8 & missing number is 5
	 * 
	 * APPROACH
	 * sum1 = whole range 1+2+3+4+5+6+7+8 = 36
	 * sum2 = given array 2+3+1+6+4+8+7 =   31 
	 * FInally diff of sum1-sum2 = missing value => 36-31 = 5       
	 */

	public static void main(String[] args) {
		
		//int a[] = {-4,-2,-1,0,1,3,2,4};
		int a[] = {2,5,4,3,8,7,9};
		int sum1 = 0, sum2 = 0;
		int min = a[0];
		int max ; int missingElement;
		
		// sum of given array elements
		for(int val : a) {
			
			//fetch minimum value in array - start point of range
			if(val < min) {
				min = val;
			}
			
			sum2 = sum2 + val; 
		}

		// fetch maximum element in array range
			max = (a.length)+ min;
		
		// sum of range from small to largest elment in arary - sum1 calculation
		for(int i=min; i<=max; i++) {
			sum1 = sum1+ i;
		}
		/*
		 * Here is the simple code to generate minimum & maximum values using streams - Array class
		 *  int min = Arrays.stream(a).min().orElseThrow();
		 *  int max = Arrays.stream(a).max().orElseThrow();
		 */
		
		missingElement = sum1 - sum2;
		
		/*
		System.out.println("sum of given array = "+sum2);
		System.out.println("sum of actual array = "+sum1);
		System.out.println("min value in given array = "+min);
		System.out.println("max value in given array = "+max);
		System.out.println("length of array = "+a.length);
		*/
		System.out.println("Misising Element = "+missingElement);
		
		
	}

}
