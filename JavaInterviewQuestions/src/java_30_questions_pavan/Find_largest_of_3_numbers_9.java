package java_30_questions_pavan;

import java.util.Scanner;

public class Find_largest_of_3_numbers_9 {
	
	/*
	 * How to find largest of 3 numbers - 2 ways
	eg: a=10, b=20, c=30
	if a>b && a>c --> a is largest, if b>a & b > c --> b is largest, c>a & c>b --> c is largest
	*/
	

	public static void main(String[] args) {
		
		
		//METHOD 1 - compare 1 with other 2 numbers
		Scanner scr = new Scanner(System.in);
		
		System.out.println("Enter First Number:");
		int a = scr.nextInt();
		
		System.out.println("Enter Second Number:");
		int b = scr.nextInt();
		
		System.out.println("Enter Third Number:");
		int c = scr.nextInt();
		
		
		if(a>b && a>c) {
			System.out.println(a+" --> A- is the Largest number");
		}else if(b>a && b>c) {
			System.out.println(b+" --> B- is the Largest number");
		}else if(c>a && c>b) {
			System.out.println(c+" C- is the largest number");
		}else {
			System.out.println("All Values are same");
		}
		
		
		//METHOD 2 - using ternary operator
		/*
		 syntax of ternary operator = condition?a:b -- if condition is true -> a will return else b will return
		 here we are comparing b/w 3 numbers so we use 2 ternary operators (a comp with b - which is hight compare with c)
		 */
		
		int largest = a>b?a:b; // largest of a & b
		largest = c>largest?c:largest; // largest & c
		
		System.out.println("largest numb = "+largest);

	}

}
