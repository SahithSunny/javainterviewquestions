package java_30_questions_pavan;

import java.util.Arrays;

public class Find_max_and_Min_Array_18 {

	public static void main(String[] args) {
	int a[] = {80,55,20,0,39,50,20};
	
	int max = a[0];
	
	for(int val:a) {
		if(val>max) {
			max = val;
		}
	}
	
	System.out.println("maximum array value = "+max);
	
	// fetch the maximum value from Array using streams
	System.out.println("max value using Streams = "+ Arrays.stream(a).max().orElseThrow());
	
	int min = a[0];
	
	for(int val: a) {
		
		if(val< min) {
			min = val;
		}
	}
	
	System.out.println("minimum array value = "+min);
	
	// fetch the minimum value from Array using streams
	System.out.println("min value using Streams = "+ Arrays.stream(a).min().orElseThrow());
	

	}

}
