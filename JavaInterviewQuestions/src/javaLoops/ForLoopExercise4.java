package javaLoops;

/*WAP to print 
1 2 3 4
2 3 4 1
3 4 1 2
4 3 2 1*/

public class ForLoopExercise4 {

	public static void main(String[] args) {
		
	for(int i=1; i<=4; i++)
	{
		for(int j=1; j<=4; j++)
		{
			System.out.print(i+" ");
			i++;
			
			if(i>=5)
			{
				i=1;
			}
		}
		System.out.println();
	}
		
	}

}
