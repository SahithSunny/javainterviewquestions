package javaLoops;

/*W.A.P to print
1  2  3  4  
5  6  7  
8  9  
10*/

public class ForLoopExercise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int k=1;
		
		for(int i=0; i<4; i++)
		{
			for(int j=1; j<=4-i; j++)
			{
				System.out.print(k+"  ");
				k++;
			}
			System.out.println();
		}
	}

}
